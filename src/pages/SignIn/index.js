import React, { useRef, useState } from 'react';
import {
  Text,
  Alert,
  TouchableOpacity,
  Image,
  ImageBackground,
  Platform,
  View,
} from 'react-native';
import { AntDesign, FontAwesome5, FontAwesome } from '@expo/vector-icons';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { Container, Button, CheckBox, StyleProvider } from 'native-base';
import Swiper from 'react-native-swiper';

import api from '../../services/Api';

import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';

import * as Google from 'expo-google-app-auth';
import * as Facebook from 'expo-facebook';
import * as AppleAuthentication from 'expo-apple-authentication';

import ModalTipoCadastro from '../../components/ModalTipoCadastro';

import getTheme from '../../native-base-theme/components';
import platform from '../../native-base-theme/variables/platform';

import Loader from '../../components/Loader';

import styles from './styles';
import backgroundImage from '../../assets/img/tela-main.png';
import logoImage from '../../assets/img/logo-tela1.png';

function SignIn({ navigation }) {
  const refAlert = useRef();
  const [handleCheck, setHandleCheck] = useState(null);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [showLoader, setShowLoader] = useState(false);

  const _loginApple = async () => {
    if (!handleCheck) {
      return Alert.alert(
        'Go Fly',
        'Escolha um tipo de usuário para fazer login',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
      );
    }
    setShowLoader(true);

    try {
      const credential = await AppleAuthentication.signInAsync({
        requestedScopes: [
          AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
          AppleAuthentication.AppleAuthenticationScope.EMAIL,
        ],
      });
      var dadosLogin = {
        nome: credential.fullName.givenName,
        email: credential.email,
        user: credential.user,
      };
      _loginAction(dadosLogin);
      // signed in
    } catch (err) {
      console.log(err);
    }
  };
  const _loginFacebook = async () => {
    if (!handleCheck) {
      return Alert.alert(
        'Go Fly',
        'Escolha um tipo de usuário para fazer login',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
      );
    }
    setShowLoader(true);
    try {
      const id = '261072444523398';

      await Facebook.initializeAsync({
        appId: id,
      });
      const {
        type,
        token,
        expirationDate,
        permissions,
        declinedPermissions,
      } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ['public_profile', 'email'],
      });

      if (type === 'success') {
        // Get the user's name using Facebook's Graph API
        const response = await fetch(
          `https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,picture.type(large)`,
        );

        const dadosUser = await response.json();
        console.log(dadosUser);

        var dadosLogin = {
          nome: dadosUser.name,
          email: dadosUser.email,
        };
        _loginAction(dadosLogin);
      } else {
        setShowLoader(false);
      }
    } catch (e) {
      alert('error:', e);
      return { error: true };
    } finally {
      setShowLoader(false);
    }
  };

  const _loginGoogle = async () => {
    if (!handleCheck) {
      return Alert.alert(
        'Go Fly',
        'Escolha um tipo de usuário para fazer login',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
      );
    }

    try {
      const clienteAndroid =
        '678018015719-o349kf3epfmitdkd2fk20jbsm307g6oa.apps.googleusercontent.com';
      const clienteIOS =
        '678018015719-cospgv91c4qvmilpvgnvok74filinu41.apps.googleusercontent.com';
      const androidStandaloneAppClientId =
        '678018015719-o349kf3epfmitdkd2fk20jbsm307g6oa.apps.googleusercontent.com';
      const iosStandaloneAppClientId =
        '678018015719-6chu1vma57lk49hshi6koeuidbg3b47u.apps.googleusercontent.com';
      const webClientId =
        '678018015719-dis5vd1lhuko2gfeh5e68iq7jn0mr6l7.apps.googleusercontent.com';

      const result = await Google.logInAsync({
        androidClientId: clienteAndroid,
        iosClientId: clienteIOS,
        androidStandaloneAppClientId: androidStandaloneAppClientId,
        iosStandaloneAppClientId: iosStandaloneAppClientId,
        webClientId: webClientId,
        scopes: ['profile', 'email'],
      });
      console.log(result);
      if (result.type === 'success') {
        var dadosLogin = {
          nome: result.user.name,
          email: result.user.email,
        };
        _loginAction(dadosLogin);
      } else {
        console.log('cancelled');
        return { cancelled: true };
      }
    } catch (e) {
      console.log('error teste: ' + e);
      return { error: true };
    }
  };

  const _loginConvidado = async () => {
    await AsyncStorage.setItem('Usuario.tipo', 'cliente');
    return navigation.navigate('App');
  };

  const _fazerCadastro = () => setIsModalVisible(!isModalVisible);

  const _toggleModal = () => setIsModalVisible(!isModalVisible);

  const _fazerLogin = () => {
    navigation.navigate('FazerLogin');
  };

  const _cadastrarCliente = () => {
    _toggleModal();
    navigation.navigate('CadastroCliente');
  };

  const _cadastrarAtleta = () => {
    _toggleModal();
    navigation.navigate('CadastroAtleta');
  };

  const _getToken = async (usuario_id, usuario_tipo) => {
    try {
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS,
      );
      let finalStatus = existingStatus;

      // only ask if permissions have not already been determined, because
      // iOS won't necessarily prompt the user a second time.
      if (existingStatus !== 'granted') {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(
          Permissions.NOTIFICATIONS,
        );
        finalStatus = status;
      }

      // Stop here if the user did not grant permissions
      if (finalStatus !== 'granted') {
        return;
      }

      // Get the token that uniquely identifies this device
      let token = await Notifications.getExpoPushTokenAsync();

      var dados = { usuario_tipo, usuario_id, token };

      const response = await api.post('/login/savar-token', dados);
      console.log(response);
      return;
    } catch (error) {
      this.refs.loading4.close();
    }
  };

  const _loginAction = async (dados) => {
    const { email, nome, user } = dados;

    setShowLoader(true);

    var dados = {
      usuarioTipo: handleCheck,
      email,
      nome,
      user,
      origem: 'social',
    };

    const response = await api.post('/login/entrar', dados);
    // console.log(response);

    if (response.data.status == 'Success') {
      if (response.data.usuario != 'user nao cadastrado') {
        await AsyncStorage.setItem(
          'Usuario',
          JSON.stringify(response.data.usuario),
        );
        await AsyncStorage.setItem('Usuario.tipo', handleCheck);
        setShowLoader(false);
        _getToken(response.data.usuario.id, handleCheck);
        navigation.navigate('App');
      } else {
        setShowLoader(false);
        if (handleCheck == 'atleta') {
          navigation.navigate('CadastroAtleta', {
            nome,
            email,
            user,
          });
        } else {
          navigation.navigate('CadastroCliente', {
            nome,
            email,
            user,
          });
        }
      }
    } else {
      setShowLoader(false);
      return Alert.alert(
        'Go Fly',
        'Houve algum erro, tente novamente mais tarde!',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
      );
    }
  };

  return (
    <StyleProvider style={getTheme(platform)}>
      <Container>
        <ImageBackground
          source={backgroundImage}
          style={{ width: '100%', height: '100%' }}>
          <View style={styles.container}>
            <View style={styles.logo}>
              <Image source={logoImage} />
            </View>

            <View style={styles.containerSlider}>
              <Swiper
                autoplay={true}
                dot={
                  <View
                    style={{
                      backgroundColor: 'rgba(235, 207, 0, 0.5)',
                      width: 10,
                      height: 10,
                      borderRadius: 9,
                      marginLeft: 8,
                      marginRight: 8,
                      marginTop: 8,
                      marginBottom: 8,
                    }}
                  />
                }
                activeDot={
                  <View
                    style={{
                      backgroundColor: '#ebcf00',
                      width: 13,
                      height: 13,
                      borderRadius: 9,
                      marginLeft: 8,
                      marginRight: 8,
                      marginTop: 8,
                      marginBottom: 8,
                    }}
                  />
                }
                paginationStyle={{
                  bottom: 5,
                }}
                width={320}
                containerStyle={styles.insideSlider}
                showsButtons={false}>
                <View style={styles.slide1}>
                  <Text style={styles.text}>
                    Uma diversão a mais de 200km/h que ficará para sempre na sua
                    memória.
                  </Text>
                </View>
                <View style={styles.slide1}>
                  <Text style={styles.text}>
                    Viva a incrível liberdade de voar com os melhores.
                  </Text>
                </View>
              </Swiper>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginVertical: 10,
              }}>
              <View style={{ flexDirection: 'row' }}>
                <CheckBox
                  style={{ marginTop: 5 }}
                  color="#FEEE00"
                  onPress={() => {
                    setHandleCheck('atleta');
                  }}
                  checked={handleCheck === 'atleta' ? true : false}
                />

                <TouchableOpacity
                  style={{
                    marginHorizontal: 20,
                    padding: 5,
                    backgroundColor: '#000',
                    borderRadius: 10,
                  }}
                  onPress={() => {
                    setHandleCheck('atleta');
                  }}>
                  <Text
                    style={{
                      color: '#FEEE00',
                      fontSize: 18,
                      fontWeight: 'bold',
                    }}>
                    Atleta
                  </Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  marginLeft: 5,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <CheckBox
                  style={{ marginTop: 5 }}
                  checkboxTickColor="#000"
                  color="#FEEE00"
                  onPress={() => {
                    setHandleCheck('cliente');
                  }}
                  checked={handleCheck === 'cliente' ? true : false}
                />
                <TouchableOpacity
                  style={{
                    marginHorizontal: 20,
                    padding: 5,
                    backgroundColor: '#000',
                    borderRadius: 10,
                  }}
                  onPress={() => {
                    setHandleCheck('cliente');
                  }}>
                  <Text
                    style={{
                      color: '#FEEE00',
                      fontSize: 18,
                      fontWeight: 'bold',
                    }}>
                    Cliente
                  </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.containerButton}>
              {Platform.OS === 'ios' && (
                <Button style={styles.buttonApple} onPress={_loginApple}>
                  <AntDesign name="apple1" size={16} color="white" />
                  <Text
                    style={{
                      fontSize: 16,
                      fontWeight: '700',
                      color: '#fff',
                      paddingLeft: 5,
                    }}>
                    Iniciar Sessão com a Apple
                  </Text>
                </Button>
              )}

              <Button
                primary
                style={styles.buttonFace}
                onPress={_loginFacebook}>

                <FontAwesome5  name="facebook-f" size={16} color="white" />

                <Text style={[styles.textButton]}> Login com Facebook </Text>
              </Button>

              <Button danger style={styles.buttonGoogle} onPress={_loginGoogle}>
                <FontAwesome name="google" size={16} color="white" />
                <Text style={styles.textButton}> Login com Google </Text>
              </Button>
            </View>

            <View style={{ flex: 1 }}>
              <TouchableOpacity onPress={_loginConvidado}>
                <Text
                  style={{
                    marginHorizontal: 50,
                    textDecorationLine: 'underline',
                    color: '#FEEE00',
                    fontSize: 20,
                    fontWeight: 'bold',
                  }}>
                  Entrar como Convidado{' '}
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.containerCadastrar}>
              <View style={{ flexDirection: 'row' }}>
                <Text
                  style={{ fontSize: 20, color: '#fff', fontWeight: 'bold' }}>
                  Ja possui conta?
                </Text>
                <TouchableOpacity
                  style={{ marginLeft: 5 }}
                  onPress={_fazerLogin}>
                  <Text
                    style={{
                      textDecorationLine: 'underline',
                      fontSize: 20,
                      color: '#fff',
                      fontWeight: 'bold',
                    }}>
                    Faca login
                  </Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity onPress={_fazerCadastro}>
                <Text
                  style={{
                    marginHorizontal: 50,
                    marginTop: 15,
                    textDecorationLine: 'underline',
                    color: '#FEEE00',
                    fontSize: 20,
                    fontWeight: 'bold',
                  }}>
                  Cadastre-se
                </Text>
              </TouchableOpacity>
            </View>
            <ModalTipoCadastro
              onRequestClose={() => {}}
              openAtleta={_cadastrarAtleta}
              openCliente={_cadastrarCliente}
              onCancel={_toggleModal}
              isVisible={isModalVisible}
            />

            <Loader show={showLoader} />
          </View>
        </ImageBackground>
      </Container>
    </StyleProvider>
  );
}

export default SignIn;
