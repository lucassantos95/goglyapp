import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerSlider: {
    flex: 3,
    // backgroundColor:'#0f0'
  },

  containerButton: {
    flex: 4,
    // backgroundColor:'#f00'
    // marginTop: 10
  },
  containerCadastrar: {
    flex: 2,
  },
  logo: {
    flex: 3,
    width: 200,
    height: 200,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: Platform.OS === 'ios' ? 30 : 0,
  },

  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },

  buttonFace: {
    marginTop: 10,
    width: 250, height: 44,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonApple: {
    backgroundColor:'#000',
    marginTop: 10,
    width: 250, height: 44,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonGoogle: {

    marginTop: 10,
    width: 250, height: 44,
    justifyContent: 'center',
    alignItems: 'center',
  },

  insideSlider: {
    paddingBottom: Platform.OS === 'android' ? 30 : 0,
    margin: 0,
  },
  slide1: {
    flex: 1,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textButton: {
    fontSize: 18,
    color: '#fff',
    flex:0.9,
    paddingLeft: 5,
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default styles;