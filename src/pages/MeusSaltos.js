import React, { Component } from 'react';
import {
  StatusBar,
  StyleSheet,
  ActivityIndicator,
  View,
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Body,
  Text,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import api from '../services/Api';

export default class MeusSaltos extends Component {
  constructor() {
    super();
    this.state = {
      saltos: null,
    };
  }

  _loadNewsData = async () => {
    const atleta_dados = await AsyncStorage.getItem('Usuario');
    var atleta_id = JSON.parse(atleta_dados);

    var dados = { params:{atleta_id: atleta_id.id} };

    const response = await api.get('/atletas-saldo/index', dados);

    if ((response.data.status = 'Success')) {
      if (response.data.saldo != '') {
        var saltos = parseInt(response.data.saldo.saltos).toString();
        
        console.log(saltos);

        var saltos1 = '';
        if (typeof saltos != 'undefined' && saltos != '') {
          if (saltos.length == 1) {
            saltos1 = '0' + saltos;
            this.setState({ saltos: saltos1.split('') });
          } else {
            saltos1 = [0, 0];
            this.setState({ saldo: saltos1.split('') });
          }
        } else {
          saltos = [0, 0];
          this.setState({ saltos });
        }
      } else {
        saltos = [0, 0];
        this.setState({ saltos });
      }
    } else {
      saltos = [0, 0];
      this.setState({ saltos });
    }
  };

  componentDidMount() {
    this._loadNewsData();
  }

  static navigationOptions = {
    title: 'Lots of features here',
  };

  render() {
    const { saltos } = this.state;

    if (saltos != null) {
      //   console.log(saltos)

      return (
        <Container>
          <Content
            contentContainerStyle={{
              alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
            }}>
            <Grid>
              <Row
                style={{
                  alignItems: 'flex-end',
                  justifyContent: 'center',
                }}>
                <View style={{ margin: 15 }}>
                  <Text style={{ fontSize: 20 }}>
                    Até agora você já realizou
                  </Text>
                </View>
              </Row>
              <Row
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  flex: 2,
                }}>
                {saltos.map((saltos, index) => {
                  return (
                    <Col
                      key={index}
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: '#000',
                        width: 100,
                        marginRight: 5,
                        height: 200,
                      }}>
                      <View key={index}>
                        <Text
                          key={index}
                          style={{
                            color: '#FEEE00',
                            fontSize: 100,
                            fontWeight: 'bold',
                          }}>
                          {saltos}
                        </Text>
                      </View>
                    </Col>
                  );
                })}
              </Row>
              <Row
                style={{
                  flex: 2,
                  justifyContent: 'center',
                }}>
                <View style={{ margin: 15 }}>
                  <Text style={{ fontSize: 20 }}>Saltos Conosco</Text>
                </View>
              </Row>
            </Grid>
          </Content>
        </Container>
      );
    } else {
      return (
        <Container>
          <Content
            contentContainerStyle={{
              alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
            }}>
            <ActivityIndicator />
          </Content>
        </Container>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
