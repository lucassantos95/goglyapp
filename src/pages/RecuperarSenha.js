import React, { Component } from 'react';
import { Container, Content, Text, Button, Item } from 'native-base';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  View,
  TextInput,
  Alert,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import api from '../services/Api';

export default class RecuperarSenha extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      msgRecuperarSenha: '',
      showText: 0,
      mtText: 0,
    };
  }
  static navigationOptions = {
    header: null,
  };
  logar = () => {
    alert('fazer login');
  };
  recuperarSenha = async () => {
    if (this.state.email == '') {
      return Alert.alert('Go Fly', 'Preencha o campo E-mail', [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ]);
    }

    var dados = { params:{email: this.state.email} };

    var response = await api.get('/login/nova-senha', dados);
    console.log(response);

    if (response.data.status == 'error') {
      return this.setState({
        msgRecuperarSenha: 'E-mail Não cadastrado!',
        showText: 40,
        mtText: 10,
      });
    } else {
      this.setState({
        msgRecuperarSenha:
          'Sucesso, você receberá um e-mail com uma senha temporária',
        showText: 40,
        mtText: 10,
      });
    }

    this.hideMsg();
  };

  hideMsg = () => {
    setInterval(() => {
      this.setState({
        msgRecuperarSenha: '',
        showText: 0,
        mtText: 0,
      });
    }, 10000);
  };
  openLogin = () => {
    this.props.navigation.push('FazerLogin');
  };

  render() {
    return (
      <Container>
        <Content
          contentContainerStyle={{
            backgroundColor: '#000',
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1,
          }}>
          <View style={{ marginBottom: 20 }}>
            <Image source={require('../assets/img/logo-tela1.png')} />
          </View>

          <View
            style={{ width: 300, marginVertical: 15, backgroundColor: '#fff' }}>
            <TextInput
              underlineColorAndroid="#fff"
              placeholder="digite seu e-mail"
              placeholderTextColor="#000"
              style={{
                height: 60,
                borderColor: '#fff',
                padding: 15,
                borderWidth: 1,
              }}
              onChangeText={(email) => this.setState({ email })}
              value={this.state.email}
              autoCapitalize="none"
              keyboardType={'email-address'}
            />
          </View>

          <View
            style={{
              marginHorizontal: 30,
              height: this.state.showText,
              marginTop: this.state.mtText,
            }}>
            <Text
              style={{
                color: '#fff',
                textAlign: 'center',
                fontSize: 15,
                fontWeight: 'bold',
              }}>
              {this.state.msgRecuperarSenha}
            </Text>
          </View>

          <View style={{ marginTop: 30 }}>
            <Button
              onPress={this.recuperarSenha}
              style={{
                width: 200,
                alignItems: 'center',
                justifyContent: 'center',
                height: 50,
                backgroundColor: '#FEEE00',
              }}
              large>
              <Text
                style={{
                  textAlign: 'center',
                  fontWeight: 'bold',
                  color: '#000000',
                }}>
                Recuperar Senha
              </Text>
            </Button>
          </View>
          <View style={{ marginTop: 40 }}>
            <TouchableOpacity
              style={{
                alignItems: 'center',
                justifyContent: 'center',
              }}
              onPress={this.openLogin}>
              <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#fff' }}>
                Fazer Login
              </Text>
            </TouchableOpacity>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  containerMain: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
