
import React, { Component } from 'react';
import { Container, Content, Text, Button } from 'native-base';
import { StyleSheet, TouchableOpacity, Image, View } from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import Header from '../components/HeaderSemAvatar';
import api from '../services/Api';

import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';


export default class FinalizacaoCadastro extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
    _getToken = async (usuario_id, usuario_tipo) => {
        try {
            const { status: existingStatus } = await Permissions.getAsync(
                Permissions.NOTIFICATIONS
            );
            let finalStatus = existingStatus;

            // only ask if permissions have not already been determined, because
            // iOS won't necessarily prompt the user a second time.
            if (existingStatus !== 'granted') {
                // Android remote notification permissions are granted during the app
                // install, so this will only ask on iOS
                const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
                finalStatus = status;
            }

            // Stop here if the user did not grant permissions
            if (finalStatus !== 'granted') {
                return;
            }

            // Get the token that uniquely identifies this device
            let token = await Notifications.getExpoPushTokenAsync();

            var dados = { usuario_tipo, usuario_id, token };

            const response = await api.post('/login/savar-token', dados);
            return;
        } catch (error) {
            this.refs.loading4.close();

        }
    }

    fazerLogin = async () => {

        const email = this.props.route.params.email;
        const tipoAtleta = this.props.route.params.tipoAtleta;

        var dados = { usuarioTipo: tipoAtleta, email, origem: 'social' };
        console.log(dados)

        const response = await api.post('/login/entrar', dados);
        console.log(response)
        if (response.data.status == 'Success') {
            await AsyncStorage.setItem('Usuario', JSON.stringify(response.data.usuario));
            await AsyncStorage.setItem('Usuario.tipo', tipoAtleta);
            this._getToken(response.data.usuario.id, tipoAtleta);
            this.props.navigation.navigate('App');
        } else {
            this.props.navigation.navigate('FazerLogin');
        }
    }

    static navigationOptions = {
        header: null
    };

    render() {
        return (

            <View style={styles.containerMain} >
                <View style={{ flex: 1, paddingTop: 200 }} >
                    <Image source={require('../assets/img/logo-tela1.png')} />
                </View>
                <View style={{ flex: 1 }} >
                    <Text style={{ color: '#fff', paddingTop: 50, fontWeight: 'bold' }} >Seu cadastro foi efetuado com sucesso!</Text>
                </View>

                <View style={{ flex: 2 }} >
                    <Button onPress={this.fazerLogin} style={{
                        width: 200, alignItems: 'center',
                        justifyContent: 'center', height: 50, backgroundColor: '#FEEE00'
                    }} large >
                        <Text style={{ textAlign: 'center', fontWeight: 'bold', color: '#000000' }} >Entrar</Text>
                    </Button>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    containerMain: {
        flex: 1,
        backgroundColor: '#000',
        alignItems: 'center',
        justifyContent: 'center',
    }

});