import React, { Component } from 'react';
import {
  ActivityIndicator,
  StatusBar,
  TouchableOpacity,
  Image,
  Linking,
} from 'react-native';

import {
  Container,
  Content,
  View,
  Text,
  Icon,
  Accordion,
  Card,
  H1,
} from 'native-base';

import api from '../services/Api';
import { hosHOSTURL } from '../services/Helpers';
import Header from '../components/HeaderAvatar';
import { Row } from 'react-native-easy-grid';

export default class NewsDetalhes extends Component {
  constructor() {
    super();
    this.state = {
      news: '',
    };
  }

  _loadNewData = async () => {
    console.log('1');
    const news_id = this.props.route.params.news_id

    var dados = { params:{ id: news_id }};

    const response = await api.get('/news/view', dados);
    console.log(response);
    if ((response.data.status = 'Success'))
      this.setState({
        news: response.data.news,
      });
  };

  componentDidMount() {
    this._loadNewData();
  }

  static navigationOptions = {
    header: null,
  };

  render() {
    const { news } = this.state;
    console.log(news.link);
    if (news != '') {
      return (
        <Container>
          <Header titulo={'News'} navigation={this.props.navigation} />
          <Content>
            <View
              style={{
                marginTop: 40,
                marginHorizontal: 20,
                alignContent: 'center',
              }}>
              <Image
                source={{ uri: hosHOSTURL + '/news/' + news.foto }}
                style={{ height: 200, width: null, flex: 1 }}
              />
            </View>

            <View
              style={{
                marginTop: 40,
                marginHorizontal: 20,
                alignContent: 'center',
              }}>
              <H1 style={{ textAlign: 'center' }}>{news.titulo}</H1>
            </View>

            <View style={{ margin: 20, alignContent: 'center' }}>
              <Text style={{ fontSize: 18, textAlign: 'justify' }}>
                {news.texto}
              </Text>

              {typeof news.link != 'undefined' && news.link != '' ? (
                <TouchableOpacity
                  onPress={() => {
                    Linking.openURL(news.link);
                  }}>
                  <Text
                    style={{
                      fontSize: 18,
                      textAlign: 'justify',
                      textDecorationLine: 'underline',
                    }}>
                    Clique aqui e confira!
                  </Text>
                </TouchableOpacity>
              ) : (
                ''
              )}
            </View>
          </Content>
        </Container>
      );
    } else {
      return (
        <Container>
          <Content
            contentContainerStyle={{
              alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
            }}>
            <ActivityIndicator />
          </Content>
        </Container>
      );
    }
  }
}
