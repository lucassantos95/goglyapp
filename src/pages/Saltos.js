import React, { Component } from 'react';
import { Text, ScrollView, StyleSheet, View } from 'react-native';

import { Container, Content, Tab, Tabs } from 'native-base';

import SaldoSaltos from './SaldoSaltos';
import MeusSaltos from './MeusSaltos';
import Fidelidade from './Fidelidade';
import Header from '../components/HeaderAvatar';

export default class Saltos extends Component {
  static navigationOptions = {
    title: 'Lots of features here',
  };

  render() {
    const tabTitle = this.props.route.params.tabTitle;
    return (
      <View>
        <Header titulo={tabTitle} navigation={this.props.navigation} />

        <Tabs tabBarUnderlineStyle={{ backgroundColor: '#ccc' }}>
          <Tab
            heading="Saldo de Saltos"
            tabStyle={{ backgroundColor: '#f7f7f7' }}
            textStyle={{ color: '#6a6a6a' }}
            activeTabStyle={{
              backgroundColor: '#f7f7f7',
              padding: 0,
              margin: 0,
            }}
            activeTextStyle={{
              padding: 0,
              margin: 0,
              color: '#045ab7',
              fontWeight: 'normal',
            }}>
            <SaldoSaltos />
          </Tab>
          <Tab
            heading="Meus Saltos"
            tabStyle={{ backgroundColor: '#f7f7f7' }}
            textStyle={{ color: '#6a6a6a' }}
            activeTabStyle={{
              backgroundColor: '#f7f7f7',
              padding: 0,
              margin: 0,
            }}
            activeTextStyle={{
              padding: 0,
              margin: 0,
              color: '#045ab7',
              fontWeight: 'normal',
            }}>
            <MeusSaltos />
          </Tab>
          {/* <Tab
            heading="Fidelidade"
            tabStyle={{ backgroundColor: '#f7f7f7' }}
            textStyle={{ color: '#6a6a6a' }}
            activeTabStyle={{
              backgroundColor: '#f7f7f7',
              padding: 0,
              margin: 0,
            }}
            activeTextStyle={{
              padding: 0,
              margin: 0,
              color: '#045ab7',
              fontWeight: 'normal',
            }}>
            <Fidelidade />
          </Tab> */}
        </Tabs>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
