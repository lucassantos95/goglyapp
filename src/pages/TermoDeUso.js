import { Container, Content, Text, View, } from 'native-base';
import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Platform } from 'react-native';
import Header from '../components/HeaderSemAvatar';


export default class TermoDeUso extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    static navigationOptions = {
        header: null
    };

    render() {
        return (
            <Container>
                <Header titulo={'Termo de Uso'}  navigation={this.props.navigation}/>
                <Content>
                    <View style={styles.containerMain} >
                        <Text style={{ textAlign: 'justify' }} >
    TODAS AS SUAS INFORMAÇÕES PESSOAIS RECOLHIDAS, SERÃO USADAS PARA O AJUDAR A TORNAR A SUA VISITA NO NOSSO SITE O MAIS PRODUTIVA E AGRADÁVEL POSSÍVEL.
    
    A GARANTIA DA CONFIDENCIALIDADE DOS DADOS PESSOAIS DOS UTILIZADORES DO NOSSO APLICATIVO É IMPORTANTE PARA NÓS.
    
    TODAS AS INFORMAÇÕES PESSOAIS RELATIVAS A MEMBROS, ASSINANTES, CLIENTES OU VISITANTES QUE USEM O NOSSO SITE SERÃO TRATADAS EM CONCORDÂNCIA COM A LEI DA PROTEÇÃO DE DADOS PESSOAIS DE 26 DE OUTUBRO DE 1998 (LEI N.º 67/98).
    
    A INFORMAÇÃO PESSOAL RECOLHIDA PODE INCLUIR O SEU NOME, E-MAIL, NÚMERO DE TELEFONE E/OU TELEMÓVEL, MORADA, DATA DE NASCIMENTO E/OU OUTROS.
    
    O USO DO APLICATIVO GO FLY PRESSUPÕE A ACEITAÇÃO DESTE ACORDO DE PRIVACIDADE.
                        </Text>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    containerMain: {
        padding: 25,
        flex: 1
    }
});