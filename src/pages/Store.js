import React, { Component } from 'react';
import {
  Text,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';

import { Container, Content, Tab, Tabs } from 'native-base';

import Acessorios from './Acessorios';
import Experiencia from './Experiencia';
import Vestuario from './Vestuario';
import Header from '../components/HeaderAvatar';

export default class Store extends Component {
  static navigationOptions = {
    title: 'Lots of features here',
  };

  render() {
    const tabTitle = this.props.route.params.tabTitle
    return (
      <View>
        <Header hasBack titulo={tabTitle} navigation={this.props.navigation} />
        <Tabs tabBarUnderlineStyle={{ backgroundColor: '#ccc', }} >
          <Tab heading="Acessórios"
            tabStyle={{ backgroundColor: '#f7f7f7' }}
            textStyle={{ color: '#6a6a6a' }}
            activeTabStyle={{ backgroundColor: '#f7f7f7', padding: 0, margin: 0, }}
            activeTextStyle={{ padding: 0, margin: 0, color: '#027afb', fontWeight: 'normal', }}
          >
            <Acessorios navigation={this.props.navigation} />
          </Tab>
          <Tab heading="Experiência"
            tabStyle={{ backgroundColor: '#f7f7f7' }}
            textStyle={{ color: '#6a6a6a' }}
            activeTabStyle={{ backgroundColor: '#f7f7f7', padding: 0, margin: 0, }}
            activeTextStyle={{ padding: 0, margin: 0, color: '#027afb', fontWeight: 'normal', }}
          >
            <Experiencia navigation={this.props.navigation} />
          </Tab>
          <Tab heading="Vestuário"
            tabStyle={{ backgroundColor: '#f7f7f7' }}
            textStyle={{ color: '#6a6a6a' }}
            activeTabStyle={{ backgroundColor: '#f7f7f7', padding: 0, margin: 0, }}
            activeTextStyle={{ padding: 0, margin: 0, color: '#027afb', fontWeight: 'normal', }}
          >
            <Vestuario navigation={this.props.navigation} />
          </Tab>
        </Tabs>
      </View>
    );
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
