import React, { Component } from 'react';
import {
  Container,
  Content,
  Text,
  Button,
  CheckBox,
  StyleProvider,
  Icon,
} from 'native-base';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  View,
  TextInput,
  Alert,
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import getTheme from '../native-base-theme/components';
import platform from '../native-base-theme/variables/platform';
import api from '../services/Api';
import { registerForPushNotificationsAsync } from '../services/Notification';
import { useNavigation, StackActions } from '@react-navigation/native';

export default class FazerLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      senha: '',
      checkAtletaOrCliente: false,
    };
  }
  static navigationOptions = {
    header: null,
  };

  checkAtletaOrCliente = (tipo) => {
    this.setState({
      checkAtletaOrCliente: tipo,
    });
  };

  _getToken = async (usuario_id, usuario_tipo) => {
    try {
      // Get the token that uniquely identifies this device
      let token = await registerForPushNotificationsAsync();

      var dados = { usuario_tipo, usuario_id, token };

      const response = await api.post('/login/savar-token', dados);
      return;
    } catch (error) {
      this.refs.loading4.close();
    }
  };

  logar = async () => {
    const { checkAtletaOrCliente, email, senha } = this.state;

    if (checkAtletaOrCliente == false) {
      return Alert.alert(
        'Go Fly',
        'Escolha um perfil de usuário para fazer login',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
      );
    }

    if (email == '') {
      return Alert.alert('Go Fly', 'Preencha o campo E-mail', [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ]);
    }

    if (senha == '') {
      return Alert.alert('Go Fly', 'Preencha o campo Senha', [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ]);
    }

    var dados = {
      usuarioTipo: checkAtletaOrCliente,
      email,
      senha,
      origem: 'nativo',
    };

    const response = await api.post('/login/entrar', dados);
    console.log(response);
    if (response.data.status == 'Success') {
      await AsyncStorage.setItem(
        'Usuario',
        JSON.stringify(response.data.usuario),
      );
      await AsyncStorage.setItem('Usuario.tipo', checkAtletaOrCliente);
      this._getToken(response.data.usuario.id, checkAtletaOrCliente);
      return this.props.navigation.dispatch(StackActions.replace('App'));
    } else {
      return Alert.alert('Go Fly', response.data.msg, [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ]);
    }
  };

  openRecuperarSenha = () => {
    this.props.navigation.push('RecuperarSenha');
  };

  render() {
    return (
      <StyleProvider style={getTheme(platform)}>
        <Container>
          <Content
            contentContainerStyle={{
              backgroundColor: '#000',
              alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
            }}>
            <View style={{ marginBottom: 20 }}>
              <Image source={require('../assets/img/logo-tela1.png')} />
            </View>
            <View
              style={{
                flexDirection: 'row',
                backgroundColor: '#000',
                justifyContent: 'space-between',
                marginVertical: 10,
              }}>
              <View style={{ flexDirection: 'row' }}>
                <CheckBox
                  checkedIcon={<Icon name="book" size={30} />}
                  color="#FEEE00"
                  onPress={() => {
                    this.checkAtletaOrCliente('atleta');
                  }}
                  checked={
                    this.state.checkAtletaOrCliente == 'atleta' ? true : false
                  }
                />

                <TouchableOpacity
                  style={{ marginHorizontal: 20 }}
                  onPress={() => {
                    this.checkAtletaOrCliente('atleta');
                  }}>
                  <Text
                    style={{ color: '#fff', fontSize: 18, fontWeight: 'bold' }}>
                    Atleta
                  </Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <CheckBox
                  checkboxTickColor="#000"
                  color="#FEEE00"
                  onPress={() => {
                    this.checkAtletaOrCliente('cliente');
                  }}
                  checked={
                    this.state.checkAtletaOrCliente == 'cliente' ? true : false
                  }
                />
                <TouchableOpacity
                  style={{ marginHorizontal: 20 }}
                  onPress={() => {
                    this.checkAtletaOrCliente('cliente');
                  }}>
                  <Text
                    style={{ color: '#fff', fontSize: 18, fontWeight: 'bold' }}>
                    Cliente
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                width: 300,
                marginVertical: 15,
                backgroundColor: '#fff',
              }}>
              <TextInput
                underlineColorAndroid="#fff"
                placeholder="digite seu e-mail"
                placeholderTextColor="#000"
                style={{
                  height: 60,
                  borderColor: '#fff',
                  padding: 15,
                  borderWidth: 1,
                }}
                onChangeText={(email) => this.setState({ email })}
                value={this.state.email}
                autoCapitalize="none"
                keyboardType={'email-address'}
              />
            </View>
            <View
              style={{
                width: 300,
                marginVertical: 15,
                backgroundColor: '#fff',
              }}>
              <TextInput
                underlineColorAndroid="#fff"
                placeholder="digite sua senha"
                placeholderTextColor="#000"
                style={{
                  height: 60,
                  borderColor: '#fff',
                  padding: 15,
                  borderWidth: 1,
                }}
                onChangeText={(senha) => this.setState({ senha })}
                value={this.state.senha}
                secureTextEntry={true}
              />
            </View>
            <View style={{ marginTop: 30 }}>
              <Button
                onPress={this.logar}
                style={{
                  width: 200,
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 50,
                  backgroundColor: '#FEEE00',
                }}
                large>
                <Text
                  style={{
                    textAlign: 'center',
                    fontWeight: 'bold',
                    color: '#000000',
                  }}>
                  Fazer Login
                </Text>
              </Button>
            </View>
            <View style={{ marginTop: 40 }}>
              <TouchableOpacity
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={this.openRecuperarSenha}>
                <Text
                  style={{ fontSize: 18, fontWeight: 'bold', color: '#fff' }}>
                  Esqueci Minha Senha
                </Text>
              </TouchableOpacity>
            </View>
          </Content>
        </Container>
      </StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
  containerMain: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
