import React, { Component } from 'react';
import {
  Text,
  StatusBar,
  StyleSheet,
  View,
  FlatList,
  Image,
  ImageBackground,
  ActivityIndicator,
  ScrollView
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import { Container, Header, Content, List, ListItem, Left, Right, Icon } from 'native-base';

import { LinearGradient } from 'expo';


import { Col, Row, Grid } from 'react-native-easy-grid';
import api from '../services/Api';
export default class Fidelidade extends Component {

  

  constructor() {
    super()
    this.state = {
      opacidadeLogo: 0,
      loadDados: false,
      totalCartao: 0,
      Cartao10: null,
      data: [
        // { id: "00", complete: true },
        // { id: "01", complete: true },
        // { id: "02", complete: false },
        // { id: "03", complete: false },
        // { id: "04", complete: false },
        // { id: "05", complete: false },
        // { id: "06", complete: false },
        // { id: "07", complete: false },
        // { id: "08", complete: false },
      ]
    };
  }

 

  _loadNewsData = async () => {

    const atleta_dados = await AsyncStorage.getItem('Usuario');
    var atleta_id = JSON.parse(atleta_dados);
    // console.log(atleta_id)
    var dados = { params:{atleta_id: atleta_id.id} };

    const response = await api.get('/fidelidade/index', dados);
    console.log(response)
    if (response.data.status == "Success") {
      var fidelidade = response.data.fidelidade.saldo;
      console.log(fidelidade)

      if (typeof fidelidade != 'undefined') {
        var racional = fidelidade / 10;
        racional = racional.toString();
        racional = racional.split('.');


        if (racional.length == 1) {
          for (let index = 1; index <= 9; index++) {

            var preenchido = false;

            var dados = { id: index, complete: preenchido }

            this.state.data.push(dados);
          }

          this.setState({ totalCartao: racional });
        } else {

          for (let index = 1; index <= 9; index++) {

            var preenchido = (index <= racional[1]) ? true : false;
            // console.log()
            this.setState({
              opacidadeLogo: racional[1] / 10
            })
            var dados = { id: index, complete: preenchido }

            this.state.data.push(dados);
          }
          this.setState({ totalCartao: racional[0] });
        }
      } else {
        for (let index = 1; index <= 9; index++) {

          var preenchido = false;

          var dados = { id: index, complete: preenchido }

          this.state.data.push(dados);
        }
        if (typeof fidelidade == 'undefined') {
          this.setState({ totalCartao: 0 });
        }
      }

    } else {
      for (let index = 1; index <= 9; index++) {

        var preenchido = false;

        var dados = { id: index, complete: preenchido }

        this.state.data.push(dados);
      }
      if (typeof fidelidade == 'undefined') {
        this.setState({ totalCartao: 0 });
      }
    }
    this.setState({ loadDados: true });
  }

  componentDidMount() {
    this._loadNewsData();
  }

  static navigationOptions = {
    title: 'Lots of features here',
  };

  render() {
    const columns = 3;

    function createRows(data, columns) {
      const rows = Math.floor(data.length / columns); // [A]
      let lastRowElements = data.length - rows * columns; // [B]
      while (lastRowElements !== columns) { // [C]
        data.push({ // [D]
          id: `empty-${lastRowElements}`,
          complete: `empty-${lastRowElements}`,
          empty: true
        });
        lastRowElements += 1; // [E]
      }
      return data; // [F]
    }

    const { loadDados, totalCartao, opacidadeLogo } = this.state;
    if (loadDados) {
      console.log(opacidadeLogo)

      
      return (
        // <Container>
        // <Content contentContainerStyle={{ marginTop: 10, marginHorizontal: 20 }} >
        <View>


          <ScrollView>
            <View style={{ paddingBottom: 80 }}>

              <View style={{ flexDirection: 'column', marginTop: 10, alignContent: "center", alignItems: "center" }}>
                <View style={{ flexDirection: 'row', }} >
                  <Text style={{ fontSize: 18 }} >Cartões Completos:  </Text>
                  <Text style={{ fontSize: 18, fontWeight: "bold" }} >{totalCartao} </Text>
                </View>
              </View>

              <View style={{ marginHorizontal: 40, height: 320, marginTop: 10 }} >
                <FlatList
                  // scrollEnabled={false}
                  style={{ padding: 0, marginBottom: 0 }}
                  data={createRows(this.state.data, columns)}
                  keyExtractor={item => item.id}
                  numColumns={3} // Número de colunas
                  renderItem={({ item }) => {
                    if (item.empty) {
                      return <View style={[styles.itemEmpty]} />;
                    }
                    if (item.complete == true) {
                      return (
                        <View style={styles.item}>
                          <Image source={require('../assets/img/icon-fidelidade.png')} />
                        </View>
                      );
                    } else {
                      return (
                        <View style={styles.item}></View>
                      );
                    }
                  }}
                />
              </View>

              <View style={{ marginRight: 50, marginTop: 5, marginBottom: 80, marginLeft:40, flexDirection: 'row', justifyContent: "space-between" }} >
                <View style={{
                  width: 95,
                  height: 90,
                  margin: 5,
                  padding: 20,
                  borderColor: '#000',
                  borderRadius: 90 / 2,
                  overflow: 'hidden',
                  borderWidth: 2,
                }}></View>

                <View style={{ paddingRight: 15, opacity:opacidadeLogo }} >
                  <Image source={require('../assets/img/fidelidade-logo.png')} />
                </View>

              </View>
            
            </View>
          </ScrollView>
        </View>
      );
    } else {
      return (
        <Container>
          <Content contentContainerStyle={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
            <ActivityIndicator />
          </Content>
        </Container>
      )
    }
  }

}

const styles = StyleSheet.create({
  item: {
    alignItems: "center",
    backgroundColor: "#fff",
    flexGrow: 1,
    margin: 6,
    padding: 10,
    width: 95,
    height: 95,
    borderColor: '#000',
    borderRadius: 95 / 2,
    overflow: 'hidden',
    borderWidth: 2,
  },
  itemEmpty: {
    backgroundColor: "transparent",
    alignItems: "center",
    flexGrow: 1,
    margin: 5,
    padding: 20,
    flexBasis: 0,
    width: 90,
    height: 90,
    borderColor: '#000',
    borderRadius: 90 / 2,
    overflow: 'hidden',
    borderWidth: 2,
  },
});

