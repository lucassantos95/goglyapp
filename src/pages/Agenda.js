import React, { Component } from 'react';
import {
  ActivityIndicator,
  StyleSheet,
  View,
  Image
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { Container, Content, List, Card, CardItem, Icon, Text, Button, Left } from 'native-base';
import api from '../services/Api';
import Header from '../components/HeaderAvatar';
import CountDownTimer from '../components/CountDownTimer';

export default class Agenda extends Component {

  constructor() {
    super()
    this.state = {
      loadDados: false,
      endCountdown: false,
      loginConvidado: false,
      pedidosList: []

    };
  }

  static navigationOptions = {
    title: 'Lots of features here',
  };

  _validaConvidado = () => {
    if (this.state.loginConvidado == true) {
      return (
        <View>
          <Text style={{ fontSize: 20 }} >Você precisa estar autenticado para acessar esse modulo, deseja fazer isso agora ?</Text>
          <View style={{ marginTop: 10 }} >
            <Button dark onPress={async () => { await AsyncStorage.clear(); this.props.navigation.navigate('Auth'); }} iconLeft>
              <Icon type="FontAwesome" name="sign-in" />
              <Text>Fazer login</Text>
            </Button>
          </View>
        </View>
      );
    } else {
      return (
        <Text style={{ fontSize: 20 }} >Você Não Tem Agenda Vinculada!</Text>
      )
    }
  }

  _loadPedidosData = async () => {

    const cliente_dados = await AsyncStorage.getItem('Usuario');
    if (cliente_dados != null) {
      var cliente_id = JSON.parse(cliente_dados);
      console.log(cliente_id)
      var dados = { params:{cliente_id: cliente_id.id }};

      const response = await api.get('/pedidos', dados);
      console.log(response)
      if (response.data.status = "Success") {

        var pedidos = response.data.pedidos;
        console.log(pedidos)

        this.setState({
          pedidosList: pedidos,
          loadDados: true
        });
      }
    } else {
      this.setState({ loginConvidado: true, loadDados: true })
    }
  }

  componentDidMount() {
    this._loadPedidosData();
  }

  _validaDataDecolagem = (data_decolagem, status) => {
    if (data_decolagem != null) {
      return (<CountDownTimer dateLimit={data_decolagem} />)
    } else {
      return (status)
    }
  }
  _validaDataDecolagem2 = (data_decolagem) => {
    if (data_decolagem != null) {
      return ('Decolagem Em:')
    } else {
      return ('Status do Pedido:')
    }
  }

  render() {
    const { pedidosList, endCountdown, loadDados } = this.state;
    const tabTitle = this.props.route.params.tabTitle
    if (loadDados) {
      console.log(pedidosList)
      if (pedidosList != '' && typeof pedidosList != 'undefined') {

        return (
          <Container>
            <Content>
              <Header  hasBack titulo={tabTitle} navigation={this.props.navigation} />
              <List dataArray={pedidosList}
                renderRow={(list) =>

                  <Card>
                    <View style={{ flexDirection: 'row' }} >
                      <View style={{ flex: 1.2, flexDirection: 'column', backgroundColor: '#f5f5f5' }} >
                        <View style={{ backgroundColor: '#FEEE00', padding: 5 }} ><Text style={{ color: '#000', fontSize: 15 }} >Experiência Agendada</Text></View>
                        <View style={{ margin: 5, alignContent: 'flex-start' }} >
                          <View>
                            <Text style={{ fontSize: 15, marginVertical: 3, }} >{this._validaDataDecolagem2(list.data_decolagem)}</Text>
                          </View>
                          <View>
                            <Text style={{ fontSize: 15, marginVertical: 3, height: endCountdown ? 0 : 40 }} >
                              {this._validaDataDecolagem(list.data_decolagem, list.status)}
                            </Text>
                          </View>
                          <View>
                            <Text style={{ fontSize: 15, marginVertical: 3, height: endCountdown ? 40 : 0 }} >
                              Encerrado
                        </Text>
                          </View>
                          <View>
                            <Text style={{ fontSize: 15, marginVertical: 3 }} >{list.aviao_nome}</Text>
                          </View>
                        </View>
                      </View>

                      <View style={{ flex: 1.4, justifyContent: 'flex-end', backgroundColor: '#e8e8e8' }} >
                        <View style={{ flexDirection: 'column', marginBottom: 10 }}>
                          <View style={{ marginTop: 5, marginLeft: 10 }} ><Text style={{ fontSize: 15 }} >{list.cliente}</Text></View>
                          <View style={{ marginTop: 5, marginLeft: 10 }} ><Text style={{ fontSize: 15 }} >{list.instrutor}</Text></View>
                        </View>

                        <View style={{ flexDirection: 'row', }}>
                          <View style={{ marginTop: 5, marginLeft: 10, marginBottom: 10, }} ><Text style={{ fontSize: 30, fontWeight: 'bold' }} >{list.experiencia}</Text></View>
                        </View>
                      </View>
                    </View>
                  </Card>
                }>
              </List>

            </Content>
          </Container>
        );
      } else {
        return (
          <Container>
            <Content>
              <Header titulo={tabTitle} navigation={this.props.navigation} />
              <View style={{ flex: 1, marginTop: 50, alignItems: "center", justifyContent: 'center' }}>
                <View style={{ marginBottom: 50 }} >
                    {this._validaConvidado()}
                </View>
                <Image source={require('../assets/img/icone-sem-content.png')} />
              </View>
            </Content>
          </Container>
        );
      }
    } else {
      return (
        <Container>
          <Content contentContainerStyle={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
            <ActivityIndicator />
          </Content>
        </Container>
      )
    }
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
