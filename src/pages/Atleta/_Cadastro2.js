import { Container, Content, Input, Item, Text, View, Icon, Picker, ListItem, CheckBox, Body, Button } from 'native-base';
import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Platform, Alert, ActivityIndicator } from 'react-native';
import Header from '../../components/HeaderSemAvatar';

import { Constants, Permissions, ImagePicker } from 'expo';

import Icon2 from 'react-native-vector-icons/FontAwesome';

import DateTimePicker from 'react-native-modal-datetime-picker';

// import the component
import { TextInputMask } from 'react-native-masked-text';

import { converterDatas, getPathSafeDatetime } from '../../services/Helpers';

import api from '../../services/Api';
import Loading from 'react-native-whc-loading';
import { CLIENT_ERROR } from 'apisauce';

export default class Cadastro2 extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isDateTimePickerVisible: false,
            imageBase64: null,
            hasCameraPermission: null,
            is_camera_visible: false,
            time: '',
            categoria: undefined,
            val_dobragem: '',
            img_cartao_reserva: '',
            numero_afiliacao: '',
            numero_saltos: '',
            senha: '',
            confirmar_senha: '',
            checkTermos: false,
            hasEquipamento: false,
            categorias: null
        };
        this.openCamera = this.openCamera.bind(this)
    }

    getCategorias = async () => {
        const response = await api.get('/atletas-alunos/categorias');
        if (response.data.error == 'Success') {
            this.setState({
                categorias: response.data.categorias
            })
        }

    }

    UNSAFE_componentWillMount() {
        this.getCategorias();
    }

    _showDateTimePicker = () => {
        if (this.state.hasEquipamento)
            this.setState({ isDateTimePickerVisible: true });
        else
            this.setState({ isDateTimePickerVisible: false });
    }

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        console.log(date);
        var data_niver = new Date(date);

        dia = data_niver.getDate().toString(),
            diaF = (dia.length == 1) ? '0' + dia : dia,
            mes = (data_niver.getMonth() + 1).toString(),
            mesF = (mes.length == 1) ? '0' + mes : mes,
            anoF = data_niver.getFullYear();

        var data_fim = diaF + "-" + mesF + "-" + anoF;
        // date = date.toString();
        // var dataTrata =  date.split('T');
        console.log(data_fim)
        this.setState({
            val_dobragem: data_fim
        })
        this._hideDateTimePicker();
    };


    pickFromCamera = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);

        console.log(status)
        if (status !== 'granted') {
            await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);
        }

        let image = await ImagePicker.launchCameraAsync({
            mediaTypes: 'Images',
            allowsEditing: true,
            aspect: [4, 3],
            base64: true,
        }).catch((error) => {
            console.log(error)
            if (error == 'Error: User rejected permissions') {
                return Alert.alert(
                    'Go Fly',
                    'Vá até as configurações do android e ative a permissão para o App acessar a camera!',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                )
            }
        });

        this.setState({
            imageBase64: image.base64
        })

    }
    openCamera = () => {
        this.props.navigation.push('CameraComponent');
    }


    onValueChange2(value) {
        console.log(value)
        if (value != 0) {
            this.setState({
                categoria: value
            });
        }
    }

    checkTermos = () => {
        this.setState({
            checkTermos: !this.state.checkTermos
        });
    }
    hasEquipamento = () => {
        this.setState({
            hasEquipamento: !this.state.hasEquipamento
        });
    }
    openTermoUso = () => {
        this.props.navigation.push('TermoDeUso');
    }
    finalizarCadastro = async () => {
        try {
            // this.refs.loading4.show();
            var {hasEquipamento, categoria, time, val_dobragem, img_cartao_reserva, numero_afiliacao, numero_saltos, senha, confirmar_senha, checkTermos, imageBase64 } = this.state;


            if (categoria == undefined) {
                this.refs.loading4.close();
                return Alert.alert(
                    'Go Fly',
                    'Eccolha uma categoria',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                )
            }
            if(hasEquipamento){
            if (val_dobragem == '') {
                return Alert.alert(
                    'Go Fly',
                    'Preencher o campo Validade da Dobragem',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                )
            } else {
                var hoje = new Date();
                var hoje_6meses = hoje.setMonth(hoje.getMonth() + 6);
                var val_dobragem_date = new Date(val_dobragem);
                hoje_6meses = new Date(hoje_6meses);
                console.log(hoje_6meses)
                console.log(val_dobragem_date)

                var arrDataExclusao = val_dobragem.split('-');
                console.log(arrDataExclusao)
                var stringFormatada = arrDataExclusao[1] + '-' + arrDataExclusao[0] + '-' + arrDataExclusao[2];
                console.log(stringFormatada)
                var dataFormatada1 = new Date(stringFormatada);
                console.log(dataFormatada1)
                if (dataFormatada1 > hoje_6meses) {
                    return Alert.alert(
                        'Go Fly',
                        'A Validade da Dobragem não pode ser maior que 6 meses',
                        [
                            { text: 'OK', onPress: () => console.log('OK Pressed') },
                        ],
                    )
                }

            }
        }
            if (numero_afiliacao == '') {
                this.refs.loading4.close();
                return Alert.alert(
                    'Go Fly',
                    'Preencher o campo N˚ de Afiliação',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                )
            }

            if (senha == '') {
                this.refs.loading4.close();
                return Alert.alert(
                    'Go Fly',
                    'Preencher o campo Data de Senha',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                )
            }

            if (confirmar_senha == '') {
                this.refs.loading4.close();
                return Alert.alert(
                    'Go Fly',
                    'Preencher o campo Data de Confirmar Senha',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                )
            }

            if (senha != confirmar_senha) {
                this.refs.loading4.close();
                return Alert.alert(
                    'Go Fly',
                    'As Senhas precisam ser iguais!',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                )
            }


            if (checkTermos == '') {
                this.refs.loading4.close();
                return Alert.alert(
                    'Go Fly',
                    'Faça um Check nos termos de contrato',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                )
            }


            var val_dobragem = converterDatas(val_dobragem);

            if (val_dobragem == false) {
                this.refs.loading4.close();
                return alert('Digite uma Data Válida!');
            }

            const { navigation } = this.props;
            const atleta_id = navigation.getParam('atleta_id', 'NO-ID');

            var dados = {
                atleta_id, categoria, time, val_dobragem, numero_afiliacao, numero_saltos, senha, imgBase64: imageBase64

            }

            const response = await api.post('/atletas-alunos/cadastro2', dados);
            console.log(response)
            this.refs.loading4.close();
            if (response.data.error == 'Success') {
                const tipoAtleta = navigation.getParam('tipoAtleta', 'NO-ID');
                const email = navigation.getParam('email', 'NO-ID');
                this.props.navigation.push('FinalizacaoCadastro', {
                    email,
                    tipoAtleta
                });
            }
        } catch (error) {
            this.refs.loading4.close();
            return Alert.alert(
                'Go Fly',
                'Desculpe, por favor tente mais tarde!',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
            )
        }
    }
    static navigationOptions = {
        header: null
    };

    criiarLista = (label, key) => {
        return (
            <Picker.Item label={label} value={key} key={key} />
        );
    }

    render() {
        const { navigation } = this.props;
        const atleta_id = navigation.getParam('atleta_id', 'NO-ID');
        const { categorias } = this.state;
        if (categorias != null) {
            console.log(categorias)

            return (
                <Container>
                    <Header titulo={'Cadastre-se'} navigation={this.props.navigation} />
                    <Content>

                        <View style={styles.containerMain} >
                            <Text style={styles.label} >CATEGORIA</Text>
                            <Item picker>
                                <Picker
                                    headerBackButtonText="Voltar"
                                    iosHeader="Escolha"
                                    mode="dropdown"
                                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                                    style={{ width: (Platform == 'ios') ? 300 : 380 }}
                                    placeholder="Escolha uma Categoria"
                                    placeholderStyle={{ color: "#bfc6ea" }}
                                    placeholderIconColor="#007aff"
                                    selectedValue={this.state.categoria}
                                    onValueChange={this.onValueChange2.bind(this)}
                                >
                                    <Picker.Item label='Escolha uma Opção' value='0' />
                                    {
                                        categorias.map((categoria, index) => {
                                            return (
                                                this.criiarLista(categoria.nome, categoria.id)
                                            )
                                        })
                                    }


                                </Picker>
                            </Item>


                            <View style={[styles.containerRow, { marginTop: 5 }]} >
                                <Text style={styles.label} >TIME</Text>
                                <Text style={styles.label} >CARTÃO DO RESERVA</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }} >
                                <View style={{ width: 200 }}>

                                    <Item regular>
                                        <Input
                                            onChangeText={(time) => this.setState({ time })}
                                            value={this.state.time}
                                        />
                                    </Item>
                                </View>
                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: "space-between", padding: 5, paddingHorizontal: 10, marginLeft: 10, borderWidth: 1, borderColor: '#e0e0e0' }} >

                                    <Icon2 style={{ fontSize: 30, paddingTop: 5, paddingRight: 3, color: '#00c200', opacity: (this.state.imageBase64 != null) ? 100 : 0 }} name="check" />


                                    <TouchableOpacity onPress={this.pickFromCamera} style={{ flexDirection: 'row', justifyContent: 'flex-end', }} >
                                        <Icon style={{ fontSize: 30, paddingTop: 5 }} name="camera" />
                                    </TouchableOpacity>
                                </View>
                            </View>

                            {/* <View style={{ flexDirection: 'row' }} > */}

                            <ListItem>

                                <CheckBox onPress={this.hasEquipamento} checked={this.state.hasEquipamento} />
                                <Body>
                                    <TouchableOpacity onPress={this.hasEquipamento} >
                                        <Text>Possui Equipamento Próprio ?</Text>
                                    </TouchableOpacity>
                                </Body>
                            </ListItem>
                            {/* </View> */}

                            <View style={[styles.containerRow, { marginTop: 5 }]} >
                                <Text style={{ fontSize: 15 }} >VALIDADE DOBRAGEM DO RESERVA *</Text>
                            </View>

                            <View style={{ flexDirection: 'row' }} >

                                <View>
                                    <Item regular>
                                        <TextInputMask
                                            refInput={(ref) => this.myDateText = ref}
                                            style={[styles.inputTeste, { backgroundColor: (!this.state.hasEquipamento ? '#eee' : '#fff') }]}
                                            type={'datetime'}
                                            underlineColorAndroid='rgba(0,0,0,0)'
                                            options={{
                                                format: 'DD-MM-YYYY'
                                            }}
                                            onTouchStart={this._showDateTimePicker}
                                            maxLength={10}
                                            value={this.state.val_dobragem}
                                            onChangeText={val_dobragem => {
                                                this.setState({ val_dobragem })
                                            }}
                                            editable={this.state.hasEquipamento}

                                        />

                                    </Item>
                                </View>


                            </View>

                            <View style={styles.containerRow2} >
                                <Text>N˚ DE AFILIAÇÃO *</Text>
                                <Text>N˚ DE SALTOS</Text>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >

                                <View style={{ width: 150 }} >
                                    <Item regular>
                                        <Input
                                            style={{ flex: 1 }}
                                            onChangeText={(numero_afiliacao) => this.setState({ numero_afiliacao })}
                                            value={this.state.numero_afiliacao}
                                            keyboardType={'numeric'}
                                        />
                                    </Item>
                                </View>
                                <View style={{ width: 150 }} >
                                    <Item regular>
                                        <Input
                                            style={{ flex: 1 }}
                                            onChangeText={(numero_saltos) => this.setState({ numero_saltos })}
                                            value={this.state.numero_saltos}
                                            keyboardType={'numeric'}
                                        />
                                    </Item>

                                </View>

                            </View>

                            <Text style={styles.label} >SENHA *</Text>
                            <Item regular>
                                <Input
                                    onChangeText={(senha) => this.setState({ senha })}
                                    value={this.state.senha}
                                    secureTextEntry={true}
                                />
                            </Item>

                            <Text style={styles.label} >CONFIRMAR SENHA *</Text>
                            <Item regular>
                                <Input
                                    onChangeText={(confirmar_senha) => this.setState({ confirmar_senha })}
                                    value={this.state.confirmar_senha}
                                    secureTextEntry={true}
                                />
                            </Item>

                            <ListItem>

                                <CheckBox onPress={this.checkTermos} checked={this.state.checkTermos} />
                                <Body>
                                    <TouchableOpacity onPress={this.openTermoUso} >
                                        <Text style={{ textDecorationLine: 'underline' }} >Li e aceitos os termos de uso</Text>
                                    </TouchableOpacity>
                                </Body>
                            </ListItem>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginVertical: 10 }} >
                                <Button onPress={this.finalizarCadastro} style={{ backgroundColor: '#FEEE00' }} large >
                                    <Text style={{ fontWeight: 'bold', color: '#000000' }} >Cadastrar</Text>
                                </Button>
                            </View>
                            <Loading ref='loading4'
                                backgroundColor={'#00000096'}
                                indicatorColor={'#fff'}

                            />
                            <DateTimePicker
                                isVisible={this.state.isDateTimePickerVisible}
                                onConfirm={this._handleDatePicked}
                                onCancel={this._hideDateTimePicker}
                                cancelTextIOS={'Cancelar'}
                                confirmTextIOS={'Confirmar'}
                                titleIOS={'Data de Nascimento'}
                            // datePickerModeAndroid={'spinner'}
                            />
                        </View>
                    </Content>
                </Container>


            );

        } else {
            return (
                <Container>
                    <Content contentContainerStyle={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
                        <ActivityIndicator />
                    </Content>
                </Container>
            )
        }
    }
}

const styles = StyleSheet.create({

    containerMain: {
        padding: 25,
        flex: 1
    },
    containerRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    containerRow2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 10
    },
    label: {
        marginVertical: 10
    },
    inputTeste: {
        height: 50,
        width: '100%',
        padding: 10,
        fontSize: 17
    },



});