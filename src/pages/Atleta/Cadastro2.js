import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Platform,
  Alert,
  ActivityIndicator,
} from 'react-native';

import {
  Container,
  Content,
  Input,
  Item,
  Text,
  View,
  Picker,
  ListItem,
  CheckBox,
  Body,
  Button,
} from 'native-base';

import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';

import Header from '../../components/HeaderSemAvatar';

import { FontAwesome,AntDesign} from '@expo/vector-icons';

import moment from 'moment';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Loader from '../../components/Loader';

// import the component
import { TextInputMask } from 'react-native-masked-text';

import { converterDatas, getPathSafeDatetime } from '../../services/Helpers';

import api from '../../services/Api';

function Cadastro2({ navigation, route }) {
  const [isDateTimePickerVisible, setIsDateTimePickerVisible] = useState(false);
  const [imageBase64, setImageBase64] = useState(null);
  const [time, setTime] = useState(null);
  const [categoria, setCategoria] = useState(null);
  const [val_dobragem, setVal_dobragem] = useState(null);
  const [numero_afiliacao, setNumero_afiliacao] = useState(null);
  const [numero_saltos, setNumero_saltos] = useState(null);
  const [senha, setSenha] = useState(null);
  const [confirmar_senha, setConfirmar_senha] = useState(null);
  const [checkTermos, setCheckTermos] = useState(false);
  const [hasEquipamento, setHasEquipamento] = useState(false);
  const [categorias, setCategorias] = useState([]);
  const [showLoader, setShowLoader] = useState(false);
  const [val_dobragemComplete, setVal_dobragemComplete] = useState(false);

  const getCategorias = async () => {
    const response = await api.get('/atletas-alunos/categorias');
    if (response.data.error == 'Success') {
      setCategorias(response.data.categorias);
    }
  };

  useEffect(() => {
    getCategorias();
  }, []);

  const _showDateTimePicker = () => {
    if (hasEquipamento) setIsDateTimePickerVisible(true);
    else setIsDateTimePickerVisible(false);
  };

  const _hideDateTimePicker = () => setIsDateTimePickerVisible(false);

  const _handleDatePicked = (date) => {
    var data_fim = moment(date).format('DD-MM-YYYY')
    setVal_dobragem(data_fim);
    setVal_dobragemComplete(date);

    _hideDateTimePicker();
  };

  
  const pickFromCamera = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);

    if (status !== 'granted') {
      return Alert.alert(
          'Go Fly',
          'Vá até as configurações do android e ative a permissão para o App acessar a camera!',
          [
              { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
      )
    }

    let result = await ImagePicker.launchCameraAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
        base64:true
      });
  
      if (!result.cancelled) {
        setImageBase64(result.base64);
      }

  };

  const onValueChange2 = (value) => {
    console.log(value);
    if (value != 0) {
      setCategoria(value);
    }
  };

  const checkTermosAction = () => {
    setCheckTermos(!checkTermos);
  };

  const hasEquipamentoAction = () => {
    setHasEquipamento(!hasEquipamento);
  };

  const openTermoUso = () => {
    navigation.navigate('TermoDeUso');
  };

  const finalizarCadastro = async () => {
    setShowLoader(true);
    try {
 
      if (!categoria) {
        return Alert.alert('Go Fly', 'Eccolha uma categoria', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }
      if (hasEquipamento) {
        if (!val_dobragem) {
          return Alert.alert(
            'Go Fly',
            'Preencher o campo Validade da Dobragem',
            [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
          );
        } else {
          var hoje = moment();
          const monthsCount = moment(val_dobragemComplete).diff(hoje, 'months',true);

          if (monthsCount > 6) {
            return Alert.alert(
              'Go Fly',
              'A Validade da Dobragem não pode ser maior que 6 meses',
              [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
            );
          }
        }
      }
      if (!numero_afiliacao) {
        return Alert.alert('Go Fly', 'Preencher o campo N˚ de Afiliação', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }

      if (!senha) {
        return Alert.alert('Go Fly', 'Preencher o campo Data de Senha', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }

      if (!confirmar_senha) {
        return Alert.alert(
          'Go Fly',
          'Preencher o campo Data de Confirmar Senha',
          [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
        );
      }

      if (senha != confirmar_senha) {
        return Alert.alert('Go Fly', 'As Senhas precisam ser iguais!', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }

      if (!checkTermos) {
        return Alert.alert('Go Fly', 'Faça um Check nos termos de contrato', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }

      var val_dobragem_handle = val_dobragem ? converterDatas(val_dobragem) : '';

      if (val_dobragem && !val_dobragem_handle) {
        return alert('Digite uma Data Válida!');
      }

      const atleta_id = route.params?.atleta_id;

      var dados = {
        atleta_id,
        categoria,
        time,
        val_dobragem,
        numero_afiliacao,
        numero_saltos,
        senha,
        imgBase64: imageBase64,
      };

      const response = await api.post('/atletas-alunos/cadastro2', dados);
      console.log(response);

      if (response.data.error == 'Success') {
        const tipoAtleta = route.params.tipoAtleta;
        const email = route.params.email;
        navigation.navigate('FinalizacaoCadastro', {
          email,
          tipoAtleta,
        });
      }
    } catch (error) {
      console.log(error)
      return Alert.alert('Go Fly', 'Desculpe, por favor tente mais tarde!', [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ]);
    } finally {
      setShowLoader(false);
    }
  };

  const criiarLista = (label, key) => {
    return <Picker.Item label={label} value={key} key={key} />;
  };

  return (
    <Container>
      <Header titulo={'Cadastre-se'} navigation={navigation} />
      <Content>
        <View style={styles.containerMain}>
          <Text style={styles.label}>CATEGORIA</Text>
          <Item picker>
            <Picker
              headerBackButtonText="Voltar"
              iosHeader="Escolha"
              mode="dropdown"
              iosIcon={<AntDesign name="arrowdown" />}
              style={{ width: Platform == 'ios' ? 300 : 380 }}
              placeholder="Escolha uma Categoria"
              placeholderStyle={{ color: '#bfc6ea' }}
              placeholderIconColor="#007aff"
              selectedValue={categoria}
              onValueChange={onValueChange2}>
              <Picker.Item label="Escolha uma Opção" value="0" />
              {categorias.map((categoria, index) => {
                return criiarLista(categoria.nome, categoria.id);
              })}
            </Picker>
          </Item>

          <View style={[styles.containerRow, { marginTop: 5 }]}>
            <Text style={styles.label}>TIME</Text>
            <Text style={styles.label}>CARTÃO DO RESERVA</Text>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ width: 200 }}>
              <Item regular>
                <Input onChangeText={setTime} value={time} />
              </Item>
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                padding: 5,
                paddingHorizontal: 10,
                marginLeft: 10,
                borderWidth: 1,
                borderColor: '#e0e0e0',
              }}>
              <AntDesign
                style={{
                  fontSize: 30,
                  paddingTop: 5,
                  paddingRight: 3,
                  color: '#00c200',
                  opacity: imageBase64  ? 100 : 0,
                }}
                name="checkcircleo"
              />
              <TouchableOpacity
                onPress={pickFromCamera}
                style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <FontAwesome style={{ fontSize: 30, paddingTop: 5 }} name="camera" />
              </TouchableOpacity>
            </View>
          </View>

          <ListItem>
            <CheckBox onPress={hasEquipamentoAction} checked={hasEquipamento} />
            <Body>
              <TouchableOpacity onPress={hasEquipamentoAction}>
                <Text>Possui Equipamento Próprio ?</Text>
              </TouchableOpacity>
            </Body>
          </ListItem>

          <View style={[styles.containerRow, { marginTop: 5 }]}>
            <Text style={{ fontSize: 15 }}>VALIDADE DOBRAGEM DO RESERVA *</Text>
          </View>

          <View style={{ flexDirection: 'row' }}>
            <View>
              <Item regular>
                <TextInputMask
                  style={[
                    styles.inputTeste,
                    {
                      backgroundColor: !hasEquipamento ? '#eee' : '#fff',
                    },
                  ]}
                  type={'datetime'}
                  underlineColorAndroid="rgba(0,0,0,0)"
                  options={{
                    format: 'DD-MM-YYYY',
                  }}
                  onTouchStart={_showDateTimePicker}
                  maxLength={10}
                  value={val_dobragem}
                  onChangeText={setVal_dobragem}
                  editable={hasEquipamento}
                />
              </Item>
            </View>
          </View>

          <View style={styles.containerRow2}>
            <Text>N˚ DE AFILIAÇÃO *</Text>
            <Text>N˚ DE SALTOS</Text>
          </View>

          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <View style={{ width: 150 }}>
              <Item regular>
                <Input
                  style={{ flex: 1 }}
                  onChangeText={setNumero_afiliacao}
                  value={numero_afiliacao}
                  keyboardType={'numeric'}
                />
              </Item>
            </View>
            <View style={{ width: 150 }}>
              <Item regular>
                <Input
                  style={{ flex: 1 }}
                  onChangeText={setNumero_saltos}
                  value={numero_saltos}
                  keyboardType={'numeric'}
                />
              </Item>
            </View>
          </View>

          <Text style={styles.label}>SENHA *</Text>
          <Item regular>
            <Input
              onChangeText={setSenha}
              value={senha}
              secureTextEntry={true}
            />
          </Item>

          <Text style={styles.label}>CONFIRMAR SENHA *</Text>
          <Item regular>
            <Input
              onChangeText={setConfirmar_senha}
              value={confirmar_senha}
              secureTextEntry={true}
            />
          </Item>

          <ListItem>
            <CheckBox onPress={checkTermosAction} checked={checkTermos} />
            <Body>
              <TouchableOpacity onPress={openTermoUso}>
                <Text style={{ textDecorationLine: 'underline' }}>
                  Li e aceitos os termos de uso
                </Text>
              </TouchableOpacity>
            </Body>
          </ListItem>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginVertical: 10,
            }}>
            <Button
              onPress={finalizarCadastro}
              style={{ backgroundColor: '#FEEE00' }}
              large>
              <Text style={{ fontWeight: 'bold', color: '#000000' }}>
                Cadastrar
              </Text>
            </Button>
          </View>
          <Loader show={showLoader} />
          <DateTimePicker
            isVisible={isDateTimePickerVisible}
            onConfirm={_handleDatePicked}
            onCancel={_hideDateTimePicker}
            cancelTextIOS={'Cancelar'}
            confirmTextIOS={'Confirmar'}
            titleIOS={'Data de Nascimento'}
          />
        </View>
      </Content>
    </Container>
  );
}

export default Cadastro2;

const styles = StyleSheet.create({

    containerMain: {
        padding: 25,
        flex: 1
    },
    containerRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    containerRow2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 10
    },
    label: {
        marginVertical: 10
    },
    inputTeste: {
        height: 50,
        width: '100%',
        padding: 10,
        fontSize: 17
    },
});