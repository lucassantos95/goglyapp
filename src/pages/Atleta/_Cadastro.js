import { Container, Content, Input, Item, Text, View, Icon, Button } from 'native-base';
import React, { Component } from 'react';
import { StyleSheet, Platform, Alert } from 'react-native';
import Header from '../../components/HeaderSemAvatar';

// import the component
import { TextInputMask } from 'react-native-masked-text'

import { converterDatas, validaCpf, onlyNumber } from '../../services/Helpers';
import Loader from '../../components/Loader';
import DateTimePicker from 'react-native-modal-datetime-picker';

import api from '../../services/Api';
import Loading from 'react-native-whc-loading';

export default class Cadastro extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDateTimePickerVisible: false,
      nome: '',
      apelido: '',
      data_nasc: '',
      cpf: '',
      email: '',
      celular: '',
      passaporte:''
    };

  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    console.log(date);
    var data_niver = new Date(date);

    dia = data_niver.getDate().toString(),
      diaF = (dia.length == 1) ? '0' + dia : dia,
      mes = (data_niver.getMonth() + 1).toString(),
      mesF = (mes.length == 1) ? '0' + mes : mes,
      anoF = data_niver.getFullYear();

    var data_fim = diaF + "-" + mesF + "-" + anoF;
    // date = date.toString();
    // var dataTrata =  date.split('T');
    console.log(data_fim)
    this.setState({
      data_nasc: data_fim
    })
    this._hideDateTimePicker();
  };

  openCadastro2 = async () => {

    try {

      var { nome, data_nasc, cpf, email, celular, apelido,passaporte } = this.state;

      if (nome == '') {
        this.refs.loading4.close();
        return Alert.alert(
          'Go Fly',
          'Preencher o campo nome',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
        )
      }

      if (data_nasc == '' && Platform.OS == 'android') {
        this.refs.loading4.close();
        return Alert.alert(
          'Go Fly',
          'Preencher o campo Data de Nascimento',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
        )
      }

      if ((cpf == '' && passaporte == '') && Platform.OS == 'android') {
        this.refs.loading4.close();
        return Alert.alert(
          'Go Fly',
          'Preencher o campo de CPF ou PASSAPORTE',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
        )
      }

      if (email == '') {
        this.refs.loading4.close();
        return Alert.alert(
          'Go Fly',
          'Preencher o campo de E-mail',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
        )
      }
      if (celular == '' && Platform.OS == 'android') {
        this.refs.loading4.close();
        return Alert.alert(
          'Go Fly',
          'Preencher o campo de Celular',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
        )
      }

      var data_nasc = converterDatas(data_nasc);
      
      if( cpf != '' ){
        var isCpf = validaCpf(cpf);
        if (isCpf == false) {
          this.refs.loading4.close();
          return alert('Digite um cpf válido !');
        } else {
          cpf = cpf;
        }
      }

      if (data_nasc == false) {
        this.refs.loading4.close();
        return alert('Digite uma Data Válida!');
      }

      

      var dados = {
        nome, data_nasc, cpf, email, celular, apelido,passaporte

      }

      const response = await api.post('/atletas-alunos/cadastro', dados);

      this.refs.loading4.close();
      if (response.data.error == 'Success') {
        this.props.navigation.navigate('CadastroAtleta2', {
          atleta_id: response.data.id,
          tipoAtleta: 'atleta',
          email: email,

        });
      } else {
        this.refs.loading4.close();
        return Alert.alert(
          'Go Fly',
          response.data.msg,
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ],
        )
      }

    } catch (error) {
      this.refs.loading4.close();
      return Alert.alert(
        'Go Fly',
        'Desculpe, por favor tente mais tarde!',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ],
      )
    }

    this.refs.loading4.close();
  }

  render() {
    return (
      <Container>
        <Header titulo={'Cadastre-se'} navigation={this.props.navigation} />
        <Content>
          <View style={styles.containerMain} >
            <View style={styles.containerRow} >
              <Text>NOME *</Text>
              <Text style={{ fontSize: 12 }} >(*) Campos Obrigatorios</Text>
            </View>
            <Item regular>
              <Input
                onChangeText={(nome) => this.setState({ nome })}
                value={this.state.nome}
              />
            </Item>
            <Text style={styles.label} >Apelido</Text>
            <Item regular>
              <Input
                onChangeText={(apelido) => this.setState({ apelido })}
                value={this.state.apelido}
              />
            </Item>
            <Text style={styles.label} >DATA DE NASCIMENTO {(Platform.OS == 'android') ? '*' : ''}</Text>
            <Item regular>

              <TextInputMask
                refInput={(ref) => this.myDateText = ref}
                style={styles.inputTeste}
                type={'datetime'}
                underlineColorAndroid='rgba(0,0,0,0)'
                options={{
                  format: 'DD-MM-YYYY'
                }}
                // onTouchStart={this._showDateTimePicker}
                maxLength={10}
                value={this.state.data_nasc}
                onChangeText={data_nasc => {
                  this.setState({ data_nasc })
                }}
              />
            </Item>

            <Text style={styles.label} >CPF</Text>
            <Item regular>
              <TextInputMask
                refInput={(ref) => this.myCpfText = ref}
                style={styles.inputTeste}
                type={'cpf'}
                underlineColorAndroid='rgba(0,0,0,0)'
                options={{
                  format: '999.999.999-99'
                }}
                maxLength={14}
                value={this.state.cpf}
                onChangeText={cpf => {
                  this.setState({ cpf })
                }}
              />
            </Item>

            <Text style={styles.label} >PASSAPORTE</Text>
            <Item regular>
              <Input
                onChangeText={(passaporte) => this.setState({ passaporte })}
                value={this.state.passaporte}
              />
            </Item>

            <Text style={styles.label} >E-MAIL *</Text>
            <Item regular>
              <Input
                onChangeText={(email) => this.setState({ email })}
                value={this.state.email}
                keyboardType={'email-address'}
              />
            </Item>
            <Text style={styles.label} >CELULAR</Text>
            <Item regular>

              <TextInputMask
                refInput={(ref) => this.myCelText = ref}
                style={styles.inputTeste}
                type={'cel-phone'}
                underlineColorAndroid='rgba(0,0,0,0)'
                options={{
                  format: '(99) 99999-9999'
                }}
                maxLength={15}
                value={this.state.celular}
                onChangeText={celular => {
                  this.setState({ celular })
                }}
              />

              <Input
                onChangeText={(celular) => this.setState({ celular })}
                value={this.state.celular}
              />
            </Item>
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginVertical: 10 }} >
              <Button iconRight light onPress={this.openCadastro2}>
                <Text>Próximo</Text>
                <Icon name='arrow-forward' />
              </Button>
            </View>
            <Loading ref='loading4'
              backgroundColor={'#00000096'}
              indicatorColor={'#fff'}

            />
            {/* <DateTimePicker
              isVisible={this.state.isDateTimePickerVisible}
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDateTimePicker}
              cancelTextIOS={'Cancelar'}
              confirmTextIOS={'Confirmar'}
              titleIOS={'Data de Nascimento'}
            // datePickerModeAndroid={'spinner'}
            /> */}

          </View>
        </Content>
      </Container>
    );
  }
}

