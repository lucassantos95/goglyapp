import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, Platform, Alert } from 'react-native';

import {
  Container,
  Content,
  Input,
  Item,
  Text,
  View,
  Icon,
  Button,
} from 'native-base';


import api from '../../services/Api';

import Header from '../../components/HeaderSemAvatar';
import Loader from '../../components/Loader';

import { TextInputMask } from 'react-native-masked-text';

import { converterDatas, validaCpf, onlyNumber } from '../../services/Helpers';

function Cadastro({ navigation, route }) {
  const [nome, setNome] = useState('');
  const [apelido, setApelido] = useState('');
  const [data_nasc, setData_nasc] = useState('');
  const [email, setEmail] = useState('');
  const [cpf, setCpf] = useState('');
  const [user, setUser] = useState(null);
  const [celular, setCelular] = useState('');
  const [passaporte, setPassaporte] = useState('');
  const [showLoader, setShowLoader] = useState(false);

  useEffect(() => {
      const nome = route.params?.nome;
      const email = route.params?.email;
      const user = route.params?.user;

      setNome(nome);
      setEmail(email);
      setUser(user);
  }, []);

  const openCadastro2 = async () => {
    try {
      setShowLoader(true);
      if (!nome) {
        setShowLoader(false);
        return Alert.alert('Go Fly', 'Preencher o campo nome', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }

      if (!cpf && !passaporte && Platform.OS == 'android') {
        setShowLoader(false);
        return Alert.alert('Go Fly', 'Preencher o campo de CPF ou PASSAPORTE', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }

      if (!email) {
        setShowLoader(false);
        return Alert.alert('Go Fly', 'Preencher o campo de E-mail', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }

      if (cpf && Platform.OS == 'android') {
        var isCpf = validaCpf(cpf);
        if (!isCpf) {
          setShowLoader(false);
          return alert('Digite um cpf válido !');
        } else {
          cpf = cpf;
        }
      }
      var data_nasc_handle;
      if(data_nasc){       
        data_nasc_handle = converterDatas(data_nasc);
        if (!data_nasc_handle) {
          setShowLoader(false);
          return alert('Digite uma Data Válida!');
        }
      }
      

      var dados = {
        nome,
        data_nasc: data_nasc_handle,
        cpf,
        email,
        celular,
        apelido,
        passaporte,
      };

      if (user) {
        dados.user = user;
      }
      
      const response = await api.post('/atletas-alunos/cadastro', dados);

      setShowLoader(false);
      if (response.data.error == 'Success') {
        navigation.navigate('CadastroAtleta2', {
          atleta_id: response.data.id,
          tipoAtleta: 'atleta',
          email: email,
        });
      } else {
        setShowLoader(false);
        return Alert.alert('Go Fly', response.data.msg, [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }
    } catch (error) {
      console.log(error);
      setShowLoader(false);
      return Alert.alert('Go Fly', 'Desculpe, por favor tente mais tarde!', [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ]);
    }

    setShowLoader(false);
  };

  return (
    <Container>
      <Header titulo={'Cadastre-se'} navigation={navigation} />
      <Content>
        <View style={styles.containerMain}>
          <View style={styles.containerRow}>
            <Text>NOME *</Text>
            <Text style={{ fontSize: 12 }}>(*) Campos Obrigatorios</Text>
          </View>
          <Item regular>
            <Input onChangeText={setNome} value={nome} />
          </Item>
          <Text style={styles.label}>Apelido</Text>
          <Item regular>
            <Input onChangeText={setApelido} value={apelido} />
          </Item>
          <Text style={styles.label}>
            DATA DE NASCIMENTO {Platform.OS == 'android' ? '*' : ''}
          </Text>
          <Item regular>
            <TextInputMask
              style={styles.inputTeste}
              type={'datetime'}
              underlineColorAndroid="rgba(0,0,0,0)"
              options={{
                format: 'DD-MM-YYYY',
              }}
              maxLength={10}
              value={data_nasc}
              onChangeText={(data_nasc) => {
                setData_nasc(data_nasc);
              }}
            />
          </Item>

          <Text style={styles.label}>CPF</Text>
          <Item regular>
            <TextInputMask
              style={styles.inputTeste}
              type={'cpf'}
              underlineColorAndroid="rgba(0,0,0,0)"
              options={{
                format: '999.999.999-99',
              }}
              maxLength={14}
              value={cpf}
              onChangeText={setCpf}
            />
          </Item>

          <Text style={styles.label}>PASSAPORTE</Text>
          <Item regular>
            <Input onChangeText={setPassaporte} value={passaporte} />
          </Item>

          <Text style={styles.label}>E-MAIL *</Text>
          <Item regular>
            <Input
              onChangeText={setEmail}
              value={email}
              autoCapitalize="none"
              keyboardType={'email-address'}
            />
          </Item>
          <Text style={styles.label}>CELULAR</Text>
          <Item regular>
            <TextInputMask
              style={styles.inputTeste}
              type={'cel-phone'}
              underlineColorAndroid="rgba(0,0,0,0)"
              options={{
                format: '(99) 99999-9999',
              }}
              maxLength={15}
              value={celular}
              onChangeText={setCelular}
            />

            <Input onChangeText={setCelular} value={celular} />
          </Item>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
              marginVertical: 10,
            }}>
            <Button
              iconRight
              style={{ backgroundColor: '#efefef' }}
              onPress={openCadastro2}>
              <Text style={{ color: '#000' }}>Próximo</Text>
              <Icon style={{ color: '#000' }} name="arrow-forward" />
            </Button>
          </View>
          <Loader show={showLoader} />
        </View>
      </Content>
    </Container>
  );
}

export default Cadastro;

const styles = StyleSheet.create({
  containerMain: {
    padding: 25,
    flex: 1,
  },
  containerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  containerRow2: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  label: {
    marginVertical: 10,
  },

  inputTeste: {
    height: 50,
    width: '100%',
    padding: 10,
    fontSize: 17,
  },
});
