import React, { Component } from 'react';
import {
    StatusBar,
    StyleSheet,
    ActivityIndicator,
    Alert
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { Container, Content, View, Text, Icon, Accordion, Card, Button } from "native-base";
import Header from '../components/HeaderAvatar';
import { Col, Row, Grid } from 'react-native-easy-grid';

import api from '../services/Api';

import CountDownTimer from '../components/CountDownTimer';

export default class DecolagemDetalhes extends Component {

    constructor() {
        super();
        this.state = {
            endCountdown:false,
            page: 'Decolagem',
            decolagem: [
                // { id: 1, dataDecolagem: '2018-01-01 15:10:00', aeronave: 'AERONAVE 1', vagas: '5', categoria: 'A1' },
            ],

            usuarios: [
                // {
                //     nome: "Diego asdadasda asd asdasd as asd asd asd asd a", tipo: "Super Vip", vagas: 2,
                //     relacionados: [{ nome: "Lucas asdasd asd asd asd as das d dsadas", tipo: "Atleta" }, { nome: "Lucas2", tipo: "Instrutor2", }]
                // },

                // {
                //     nome: "Lucas adsad", tipo: "Atleta", vagas: 2,
                //     relacionados: [{ nome: "Diego asd ad", tipo: "Instrutor" }, { nome: "Danilo", tipo: "Instrutor2", }]
                // },

                // { nome: "Diego", tipo: "Atleta asd a", vagas: 0, relacionados: 0 }
            ]
        };
        this.excuteTimer = this.excuteTimer.bind(this)
    }

    static navigationOptions = {
        header: null
    };



    excuteTimer(id) {
        // alert(id)
        this.setState({
          endCountdown:true
        })
      }

    _loadNewData = async () => {

        const decolagem_id = this.props.route.params.manifesto_id

        console.log(decolagem_id);

        var dados = { params:{id: decolagem_id }};
        console.log(dados);
        const response = await api.get('/decolagens/view', dados);
        console.log(response)
        if (response.data.status = "Success")
            this.setState({
                decolagem: response.data.result.decolagem,
                usuarios: response.data.result.corpo
            });
    }

    componentDidMount() {
        this._loadNewData();
    }

    onTimeElapsed() {
        this.setState({
          endCountdown:true
        })
      }

    manifestar = async (id) => {

        const decolagem_id = this.props.route.params.manifesto_id

        const userToken = await AsyncStorage.getItem('Usuario');
        console.log(userToken)
        var user = JSON.parse(userToken);

        var dados = { decolagem_id: decolagem_id, usuario_id: user.id };
        console.log(dados);
        const response = await api.post('/decolagens/manifestar', dados);
        console.log(response)
        if (response.data.status = "Success") {

            Alert.alert(
                'Go Fly',
                'Manifesto realizado com sucesso!',
                [
                    { text: 'OK', onPress: () => this._loadNewData() },
                ],
            )

        }
    }

    _renderHeader(context, expanded) {
        console.log(context.relacionados)
        return (

            // <Grid>
            <Row style={{ borderColor: 'black', borderBottomWidth: 1, padding: 10, backgroundColor: "#EFEDED" }} >
                <Col style={{ width: 140 }}>
                    <Text style={{ backgroundColor: "#EFEDED", fontWeight: "500" }} numberOfLines={2} >
                        {" "}{context.nome}
                    </Text>
                </Col>
                <Col style={{ width: 100 }}>
                    <Text style={{ backgroundColor: "#EFEDED", fontWeight: "500" }}>
                        {" "}{context.tipo}
                    </Text>
                </Col>
                <Col style={{ width: 80 }}>
                    <Text style={{ textAlign: 'center', left: 15, backgroundColor: "#EFEDED", fontWeight: "500" }}>
                        {" "}{context.vagas}
                    </Text>
                </Col>
                <Col style={{ width: 30 }}>
                    {expanded
                        ? <Icon style={{ textAlign: 'right', fontSize: 18, opacity: (context.relacionados != 0) ? 100 : 0 }} name="remove-circle" />
                        : <Icon style={{ textAlign: 'right', fontSize: 18, opacity: (context.relacionados != 0) ? 100 : 0 }} name="add-circle" />}
                </Col>

            </Row>
            // </Grid>

        );
    }
    _renderContent(context) {
        // var dadosR = context.relacionados;
        console.log(context.relacionados)
        return (

            <View>
                {context.relacionados != 0 ? (
                    <View>
                        {context.relacionados.map(function (relacionado) {

                            { console.log(relacionado) }
                            return (

                                <View key={relacionado.id} style={{ flexDirection: "row", borderColor: 'black', borderBottomWidth: 1, padding: 10, alignItems: "center", backgroundColor: "#F7F7F7" }}>
                                    <View style={{ width: 140 }}>
                                        <Text style={{ backgroundColor: "#F7F7F7", }} numberOfLines={2} >{relacionado.nome}</Text>
                                    </View>

                                    <View style={{ width: 100 }}>
                                        <Text style={{ backgroundColor: "#F7F7F7", }}>{relacionado.tipo}</Text>
                                    </View>

                                    <View style={{ width: 80, opacity: 0, }}>
                                        <Text style={{ textAlign: 'center', left: 15, backgroundColor: "#F7F7F7", }}>{" "}</Text>
                                    </View>

                                    <View style={{ width: 50 }}>
                                        <Text style={{ backgroundColor: "#EFEDED", opacity: 0, }}>{" "}</Text>
                                    </View>

                                </View>
                            )
                        })}
                    </View>
                ) : (
                        <View></View>
                    )}



            </View >
        );
    }

    render() {
        const { decolagem, usuarios,endCountdown } = this.state;

        if (decolagem != '') {
            console.log(usuarios)

            return (
                <Container>
                    <Header titulo={this.state.page} navigation={this.props.navigation} action={'decolagemList'} />
                    <Content>
                        <Card>
                            <View style={{ flexDirection: 'row' }} >

                                <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#f5f5f5' }} >
                                    <View style={{ backgroundColor: '#FEEE00', padding: 5 }} ><Text style={{ color: '#000', fontSize: 15 }} >N˚ Decolagem: {decolagem.numero_decolagem} </Text></View>
                                    <View style={{ margin: 10 }} >
                                        <Text style={{ fontSize: 15, marginVertical: 3 }} >Decolagem Em</Text>
                
                                        <Text style={{ fontSize: 15, marginVertical: 3, height: endCountdown ? 0 : 40 }} >
                                            <CountDownTimer excuteTimer={this.excuteTimer}  dateLimit={decolagem.data_decolagem}  decolagem_id={decolagem.id} />
                                        </Text>
                                        <Text style={{ fontSize: 15, marginVertical: 3, height: endCountdown ? 40 : 0 }} >
                                            Encerrado
                                        </Text>

                                        <Text style={{ fontSize: 15, marginVertical: 3 }} >{decolagem.aviao_nome}</Text>
                                    </View>
                                </View>

                                <View style={{ flex: 1.4, alignItems: 'center', justifyContent: 'center', backgroundColor: '#e8e8e8' }} >
                                    {/* <View style={{ flexDirection: 'row', marginBottom: 10}}>
                                        <Button onPress={() => { this.manifestar(1) }} style={{
                                            backgroundColor: '#FEEE00', width: 200, alignItems: 'center', justifyContent: 'center', borderWidth: 1,
                                            borderColor: '#ddd',
                                            borderBottomWidth: 0,
                                            shadowColor: '#000',
                                            shadowOffset: { width: 0, height: 2 },
                                            shadowOpacity: 0.8,
                                            shadowRadius: 2,
                                            elevation: 1,
                                        }} rounded >
                                            <Text style={{ color: '#000', fontWeight: 'bold' }} >MANIFESTAR</Text>
                                        </Button>
                                    </View> */}
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{
                                            backgroundColor: '#FEEE00', width: 70,
                                            height: 70,
                                            borderColor: '#000',
                                            borderRadius: 100,
                                            overflow: 'hidden',
                                            borderWidth: 1,
                                            alignItems: 'center', justifyContent: 'center',
                                        }} >
                                            <Text style={{ fontSize: 30, fontWeight: 'bold' }} > {decolagem.vagas_restantes} </Text>
                                        </View>
                                        <View style={{ marginTop: 15, marginLeft: 10 }} ><Text style={{ fontSize: 25 }} >Vagas</Text></View>
                                    </View>
                                </View>
                            </View>
                        </Card>

                        <View style={{ flexDirection: "row", borderColor: 'black', borderBottomWidth: 1, padding: 10, alignItems: "center", backgroundColor: "#EFEDED" }}>
                            <View style={{ width: 140 }}>
                                <Text style={{ backgroundColor: "#EFEDED", fontWeight: "500" }}>{" "}Nome</Text>
                            </View>

                            <View style={{ width: 100 }}>
                                <Text style={{ backgroundColor: "#EFEDED", fontWeight: "500" }}>{" "}Tipo / Func</Text>
                            </View>

                            <View style={{ width: 80 }}>
                                <Text style={{ textAlign: 'center', left: 15, backgroundColor: "#EFEDED", fontWeight: "500" }}>{" "}Vagas</Text>
                            </View>

                            <View style={{ width: 50 }}>
                                <Text style={{ backgroundColor: "#EFEDED", opacity: 0, }}>{" "}Vagas</Text>
                            </View>

                        </View>

                        <Accordion
                            dataArray={usuarios}
                            renderHeader={this._renderHeader}
                            renderContent={this._renderContent}
                        />


                    </Content>
                </Container >
            );
        } else {
            return (
                <Container>
                    <Content contentContainerStyle={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
                        <ActivityIndicator />
                    </Content>
                </Container>
            )
        }
    }

}

// const styles = StyleSheet.create({
//     container: {
//       flex: 1,
//       backgroundColor: '#F5FCFF',
//     //   paddingTop: Constants.statusBarHeight,
//     },
//     title: {
//       textAlign: 'center',
//       fontSize: 22,
//       fontWeight: '300',
//       marginBottom: 20,
//     },
//     header: {
//       backgroundColor: '#F5FCFF',
//       padding: 10,
//     },
//     headerText: {
//       textAlign: 'center',
//       fontSize: 16,
//       fontWeight: '500',
//     },
//     content: {
//       padding: 20,
//       backgroundColor: '#fff',
//     },
//   });