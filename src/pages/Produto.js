import React, { Component } from 'react';
import {
    StatusBar,
    StyleSheet,
    Platform,
    Image
} from 'react-native';

import { Container, Content, View, Text, Icon, Accordion, Button } from "native-base";
import Header from '../components/HeaderAvatar';
import Swiper from 'react-native-swiper';

export default class Produto extends Component {

    constructor() {
        super();
        this.state = {
            page: 'Store',
            data: [
                {
                    id: 1,
                    title: 'Capacete',
                    description: 'Printing and typesetting industry. Lorem Ipsum has been the industry Lorem Ipsum has been the industry Lorem Ipsum has been the industry Lorem Ipsum has been the industry',
                    valor: '80.00',
                    qtde_estoque: 2,
                    foto: [{ url: 'https://banner2.kisspng.com/20180530/ffz/kisspng-motorcycle-helmets-arai-helmet-limited-scooter-5b0f3c647dd836.7804196015277251565155.jpg' }, { url: 'https://banner2.kisspng.com/20180530/ffz/kisspng-motorcycle-helmets-arai-helmet-limited-scooter-5b0f3c647dd836.7804196015277251565155.jpg' }]
                },
            ],


        };
    }

    static navigationOptions = {
        header: null
    };

    comprar = (id) => {
        alert(id)
    }

    render() {
        console.log(this.state.data)
        var fotos = this.state.data[0].foto;




        return (
            <Container>
                <Header titulo={this.state.page} navigation={this.props.navigation} />
                <Content>
                    <View style={styles.containerSlider}>
                        <Swiper autoplay={false}
                            dot={<View style={{ backgroundColor: 'rgba(235, 207, 0, 0.5)', width: 10, height: 10, borderRadius: 9, marginLeft: 8, marginRight: 8, marginTop: 8, marginBottom: 8 }} />}
                            activeDot={<View style={{ backgroundColor: '#ebcf00', width: 13, height: 13, borderRadius: 9, marginLeft: 8, marginRight: 8, marginTop: 8, marginBottom: 8 }} />}
                            paginationStyle={{ bottom: 5 }}
                            nextButton={<Image style={styles.image} source={require('../assets/img/nextIcon.png')} />}
                            prevButton={<Image style={styles.image} source={require('../assets/img/prevIcon.png')} />}
                            showsButtons={true}>

                            {fotos.map((foto) => (
                                <View style={styles.slide1}>
                                    <Image
                                        style={{ width: 150, height: 150 }}
                                        source={{ uri: foto.url }}
                                    />
                                </View>

                            ))}

                        </Swiper>
                    </View>

                    <View style={{ borderBottomWidth: 1, borderColor: '#D0D0D0', marginHorizontal: 20 }} />
                  <View style={{marginHorizontal: 20}}>

                    <View style={{marginVertical:20}} >
                        <Text style={{fontSize:25}}>Titulo</Text>
                    </View>

                    <View style={{marginVertical:20}} >
                        <Text>da asd asd as as asdasdasa s das dasd as dasd asdasd asd </Text>
                    </View>
                    <View style={{marginVertical:10}} >
                        <Text>satus Estoque</Text>
                    </View>
                    <View style={{ flexDirection: 'row' , justifyContent:'space-between',marginTop:50}} >
                        <View style={{marginTop:5}}>
                            <Text style={{ fontSize:35 }} >R$ 487,00</Text>
                        </View>
                        <View style={{marginLeft:10}} >
                            <Button onPress={() => { this.comprar(this.state.data[0].id) }} style={{  backgroundColor: '#FEEE00' }} large rounded >
                                <Text style={{ color: '#000' }} >COMPRAR</Text>
                            </Button>
                        </View>
                    </View>
                  </View>
                </Content>
            </Container >
        );
    }

}


const styles = StyleSheet.create({
    wrapper: {
    },
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#fff',
    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#97CAE5',
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#92BBD9',
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    containerSlider: {
        height: 250
    }
})