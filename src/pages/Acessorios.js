import React, { Component } from 'react';
import { StyleSheet, View, ActivityIndicator, Image } from 'react-native';
import { Container, Content, List, Text } from 'native-base';
import CardProduct from '../components/CardProduct';
import ActivityLoader from '../components/ActivityLoader';
import NoContent from '../components/NoContent';

import api from '../services/Api';

export default class Acessorios extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadDados: false,
      produtos: null,
    };
  }

  static navigationOptions = {
    title: 'Lots of features here',
  };

  _loadData = async () => {
    var dados = { params:{categoria: 'acessorios'} };

    const response = await api.get('/store-produtos', dados);

    if (response.data.status == 'Success') {
      this.setState({
        produtos: response.data.produtos,
      });
    }

    this.setState({
      loadDados: true,
    });
  };

  componentDidMount() {
    this._loadData();
  }

  detalhesNew = (id) => {
    this.props.navigation.push('Produto');
  };
  render() {
    const { loadDados, produtos } = this.state;

    if (!loadDados) {
      return <ActivityLoader />;
    }
    if (!produtos) {
      return <NoContent name={'Acessórios'} />;
    }
    return (
      <Container style={{ paddingBottom: 200 }}>
        <Content>
          <List
            style={{ flex: 1 }}
            dataArray={produtos}
            renderRow={(produto) => <CardProduct produto={produto} />}></List>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
