import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Linking,
  Image,
  ActivityIndicator,
} from 'react-native';
import { Container, Content, List, Text } from 'native-base';
import CardProduct from '../components/CardProduct';
import ActivityLoader from '../components/ActivityLoader';
import NoContent from '../components/NoContent';

import api from '../services/Api';

export default class Vestuario extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadDados: false,
      produtos: null,
    };
  }

  _loadData = async () => {
    var dados = { params:{categoria: 'vestuario'} };

    const response = await api.get('/store-produtos', dados);

    if (response.data.status == 'Success') {
      this.setState({
        produtos: response.data.produtos,
      });
    }

    this.setState({
      loadDados: true,
    });
  };

  componentDidMount() {
    this._loadData();
  }

  render() {
    const { loadDados, produtos } = this.state;

    if (!loadDados) {
      return <ActivityLoader />;
    }
    if (!produtos) {
      return <NoContent name={'Vestuário'} />;
    }
    return (
      <Container style={{ paddingBottom: 200 }}>
        <Content>
          <List
            style={{ flex: 1 }}
            dataArray={produtos}
            renderRow={(produto) => <CardProduct produto={produto} />}></List>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
