import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  StyleSheet,
  ScrollView,
  Text,
  Alert,
  Platform,
  TouchableOpacity,
  Image,
  ImageBackground,
  View,
} from 'react-native';

import { Facebook } from 'expo';
import { Google } from 'expo';
import * as AppleAuthentication from 'expo-apple-authentication';

import Swiper from 'react-native-swiper';
import {
  Container,
  Content,
  Button,
  CheckBox,
  StyleProvider,
  Icon,
} from 'native-base';
import ModalTipoCadastro from '../components/ModalTipoCadastro';

import getTheme from '../native-base-theme/components';
import platform from '../native-base-theme/variables/platform';
import api from '../services/Api';
import { registerForPushNotificationsAsync } from '../services/Notification';

import Loading from 'react-native-whc-loading';

export default class SignInScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      width: '99',
      isModalVisible: false,
      checkAtletaOrCliente: false,
      CustomAlert: false,
    };
  }

  _fazerCadastro = async () => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
    });
  };

  _getToken = async (usuario_id, usuario_tipo) => {
    try {
      // Get the token that uniquely identifies this device
      let token = await registerForPushNotificationsAsync();

      var dados = { usuario_tipo, usuario_id, token };

      const response = await api.post('/login/savar-token', dados);
      console.log(response);
      return;
    } catch (error) {
      this.refs.loading4.close();
    }
  };

  _addRepository = (data) => {
    alert(data);
  };

  _cadastrarAtleta = () => {
    this._toggleModal();
    this.props.navigation.push('CadastroAtleta');
  };

  _cadastrarCliente = () => {
    this._toggleModal();
    this.props.navigation.push('CadastroCliente');
  };

  _modalVisible = () => {
    this.setState({
      modalVisible: true,
    });
  };
  showAlert = () => {
    this.setState({
      CustomAlert: true,
    });
  };

  hideAlert = () => {
    this.setState({
      CustomAlert: false,
    });
  };

  _loginAction = async (dados) => {
    this.refs.loading4.show();
    const { checkAtletaOrCliente } = this.state;
    const { email, nome } = dados;

    var dados = {
      usuarioTipo: checkAtletaOrCliente,
      email,
      nome,
      origem: 'social',
    };

    const response = await api.post('/login/entrar', dados);
    // console.log(response);

    if (response.data.status == 'Success') {
      if (response.data.usuario != 'user nao cadastrado') {
        await AsyncStorage.setItem(
          'Usuario',
          JSON.stringify(response.data.usuario),
        );
        await AsyncStorage.setItem('Usuario.tipo', checkAtletaOrCliente);

        this._getToken(response.data.usuario.id, checkAtletaOrCliente);
        this.refs.loading4.close();
        this.props.navigation.navigate('App');
      } else {
        this.refs.loading4.close();
        if (checkAtletaOrCliente == 'atleta') {
          this.props.navigation.navigate('CadastroAtleta', {
            nome,
            email,
          });
        } else {
          this.props.navigation.navigate('CadastroCliente', {
            nome,
            email,
          });
        }
      }
    } else {
      this.refs.loading4.close();
      return Alert.alert(
        'Go Fly',
        'Houve algum erro, tente novamente mais tarde!',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
      );
    }
  };

  _loginFacebook = async () => {
    if (this.state.checkAtletaOrCliente == false) {
      return Alert.alert(
        'Go Fly',
        'Escolha um tipo de usuário para fazer login',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
      );
    }

    try {
      const id = '261072444523398';

      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions,
      } = await Facebook.logInWithReadPermissionsAsync(id, {
        permissions: ['public_profile', 'email'],
      });

      if (type === 'success') {
        // Get the user's name using Facebook's Graph API
        const response = await fetch(
          `https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,picture.type(large)`,
        );

        const dadosUser = await response.json();

        var dadosLogin = {
          nome: dadosUser.name,
          email: dadosUser.email,
        };
        this._loginAction(dadosLogin);
      } else {
        this.refs.loading4.close();
      }
    } catch (e) {
      this.refs.loading4.close();
      alert('error:', e);
      return { error: true };
    }
  };

  _loginConvidado = async () => {
    await AsyncStorage.setItem('Usuario.tipo', 'cliente');
    return this.props.navigation.navigate('App');
  };

  _loginGoogle = async () => {
    if (this.state.checkAtletaOrCliente == false) {
      return Alert.alert(
        'Go Fly',
        'Escolha um tipo de usuário para fazer login',
        [{ text: 'OK', onPress: () => console.log('OK Pressed') }],
      );
    }

    try {
      const clienteAndroid =
        '678018015719-o349kf3epfmitdkd2fk20jbsm307g6oa.apps.googleusercontent.com';
      const clienteIOS =
        '678018015719-6chu1vma57lk49hshi6koeuidbg3b47u.apps.googleusercontent.com';
      const androidStandaloneAppClientId =
        '678018015719-o349kf3epfmitdkd2fk20jbsm307g6oa.apps.googleusercontent.com';
      const iosStandaloneAppClientId =
        '678018015719-6chu1vma57lk49hshi6koeuidbg3b47u.apps.googleusercontent.com';
      const webClientId =
        '678018015719-dis5vd1lhuko2gfeh5e68iq7jn0mr6l7.apps.googleusercontent.com';

      const result = await Google.logInAsync({
        androidClientId: clienteAndroid,
        iosClientId: clienteIOS,
        androidStandaloneAppClientId: androidStandaloneAppClientId,
        iosStandaloneAppClientId: iosStandaloneAppClientId,
        webClientId: webClientId,
        scopes: ['profile', 'email'],
      });

      if (result.type === 'success') {
        var dadosLogin = {
          nome: result.user.name,
          email: result.user.email,
        };
        this._loginAction(dadosLogin);
      } else {
        console.log('cancelled');
        return { cancelled: true };
      }
    } catch (e) {
      console.log('error teste: ' + e);
      return { error: true };
    }
  };

  render() {
    return (
      <StyleProvider style={getTheme(platform)}>
        <Container>
          <ImageBackground
            source={require('../assets/img/tela-main.png')}
            style={{ width: '100%', height: '100%' }}>
            <View style={styles.container}>
              <View style={styles.logo}>
                <Image source={require('../assets/img/logo-tela1.png')} />
              </View>

              <View style={styles.containerSlider}>
                <Swiper
                  autoplay={true}
                  dot={
                    <View
                      style={{
                        backgroundColor: 'rgba(235, 207, 0, 0.5)',
                        width: 10,
                        height: 10,
                        borderRadius: 9,
                        marginLeft: 8,
                        marginRight: 8,
                        marginTop: 8,
                        marginBottom: 8,
                      }}
                    />
                  }
                  activeDot={
                    <View
                      style={{
                        backgroundColor: '#ebcf00',
                        width: 13,
                        height: 13,
                        borderRadius: 9,
                        marginLeft: 8,
                        marginRight: 8,
                        marginTop: 8,
                        marginBottom: 8,
                      }}
                    />
                  }
                  paginationStyle={{
                    bottom: 5,
                  }}
                  width={320}
                  containerStyle={styles.insideSlider}
                  showsButtons={false}>
                  <View style={styles.slide1}>
                    <Text style={styles.text}>
                      Uma diversão a mais de 200km/h que ficará para sempre na
                      sua memória.
                    </Text>
                  </View>
                  <View style={styles.slide1}>
                    <Text style={styles.text}>
                      Viva a incrível liberdade de voar com os melhores.
                    </Text>
                  </View>
                </Swiper>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginVertical: 10,
                }}>
                <View style={{ flexDirection: 'row' }}>
                  <CheckBox
                    style={{ marginTop: 5 }}
                    color="#FEEE00"
                    onPress={() => {
                      this.checkAtletaOrCliente('atleta');
                    }}
                    checked={
                      this.state.checkAtletaOrCliente == 'atleta' ? true : false
                    }
                  />

                  <TouchableOpacity
                    style={{
                      marginHorizontal: 20,
                      padding: 5,
                      backgroundColor: '#000',
                      borderRadius: 10,
                    }}
                    onPress={() => {
                      this.checkAtletaOrCliente('atleta');
                    }}>
                    <Text
                      style={{
                        color: '#FEEE00',
                        fontSize: 18,
                        fontWeight: 'bold',
                      }}>
                      Atleta
                    </Text>
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    marginLeft: 5,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <CheckBox
                    style={{ marginTop: 5 }}
                    checkboxTickColor="#000"
                    color="#FEEE00"
                    onPress={() => {
                      this.checkAtletaOrCliente('cliente');
                    }}
                    checked={
                      this.state.checkAtletaOrCliente == 'cliente'
                        ? true
                        : false
                    }
                  />
                  <TouchableOpacity
                    style={{
                      marginHorizontal: 20,
                      padding: 5,
                      backgroundColor: '#000',
                      borderRadius: 10,
                    }}
                    onPress={() => {
                      this.checkAtletaOrCliente('cliente');
                    }}>
                    <Text
                      style={{
                        color: '#FEEE00',
                        fontSize: 18,
                        fontWeight: 'bold',
                      }}>
                      Cliente
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={styles.containerButton}>
                <AppleAuthentication.AppleAuthenticationButton
                  buttonType={
                    AppleAuthentication.AppleAuthenticationButtonType.SIGN_IN
                  }
                  buttonStyle={
                    AppleAuthentication.AppleAuthenticationButtonStyle.BLACK
                  }
                  cornerRadius={5}
                  style={{ width: 200, height: 44 }}
                  onPress={async () => {
                    try {
                      const credential = await AppleAuthentication.signInAsync({
                        requestedScopes: [
                          AppleAuthentication.AppleAuthenticationScope
                            .FULL_NAME,
                          AppleAuthentication.AppleAuthenticationScope.EMAIL,
                        ],
                      });
                      // signed in
                    } catch (e) {
                      if (e.code === 'ERR_CANCELED') {
                        // handle that the user canceled the sign-in flow
                      } else {
                        // handle other errors
                      }
                    }
                  }}
                />
              
               
              </View>

              <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={this._loginConvidado}>
                  <Text
                    style={{
                      marginHorizontal: 50,
                      textDecorationLine: 'underline',
                      color: '#FEEE00',
                      fontSize: 20,
                      fontWeight: 'bold',
                    }}>
                    Entrar como Convidado{' '}
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={styles.containerCadastrar}>
                <View style={{ flexDirection: 'row' }}>
                  <Text
                    style={{ fontSize: 20, color: '#fff', fontWeight: 'bold' }}>
                    Ja possui conta?
                  </Text>
                  <TouchableOpacity
                    style={{ marginLeft: 5 }}
                    onPress={this._fazerLogin}>
                    <Text
                      style={{
                        textDecorationLine: 'underline',
                        fontSize: 20,
                        color: '#fff',
                        fontWeight: 'bold',
                      }}>
                      Faca login
                    </Text>
                  </TouchableOpacity>
                </View>
                <TouchableOpacity onPress={this._fazerCadastro}>
                  <Text
                    style={{
                      marginHorizontal: 50,
                      marginTop: 15,
                      textDecorationLine: 'underline',
                      color: '#FEEE00',
                      fontSize: 20,
                      fontWeight: 'bold',
                    }}>
                    Cadastre-se
                  </Text>
                </TouchableOpacity>
              </View>
              <ModalTipoCadastro
                onRequestClose={() => {}}
                openAtleta={this._cadastrarAtleta}
                openCliente={this._cadastrarCliente}
                onCancel={this._toggleModal}
                isVisible={this.state.isModalVisible}
              />

              <Loading
                ref="loading4"
                backgroundColor={'#00000096'}
                indicatorColor={'#fff'}
              />
            </View>
          </ImageBackground>
        </Container>
      </StyleProvider>
    );
  }

  _signInAsync = async () => {
    await AsyncStorage.setItem('userToken', 'abc');
    this.props.navigation.navigate('App');
  };
}
