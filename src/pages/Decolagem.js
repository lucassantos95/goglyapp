import React, { Component } from 'react';
import {
  ScrollView,
  StyleSheet,
  Image,
  ActivityIndicator,
} from 'react-native';

import { Container, Content, List, View, Card, CardItem, Body, Text, Thumbnail, Button } from 'native-base';

import api from '../services/Api';

import CountDownTimer from '../components/CountDownTimer';
import Header from '../components/HeaderAvatar';

export default class Decolagem extends Component {
  constructor() {
    super()
    this.state = {
      loadDados: false,
      endCountdown: false,
      decolagemList: [
        // { id: 1, dataDecolagem: '2018-01-01 15:10:00', aeronave: 'AERONAVE 1', vagas: '5', categoria: 'A1' },
        // { id: 1, dataDecolagem: '2018-01-01 15:10:00', aeronave: 'AERONAVE 1', vagas: '5', categoria: 'A1' },
      ]
    };
    this.excuteTimer = this.excuteTimer.bind(this)
  }

  _loadManifestoData = async () => {
    const response = await api.get('/decolagens');
    console.log(response)
    if (response.data.status = "Success") {

      var descolagens = response.data.manifestos;
      console.log(descolagens)

      this.setState({
        decolagemList: descolagens,
        loadDados: true
      });
    }

  }

  componentDidMount() {
    this._loadManifestoData();
  }

  detalhesNew = (id) => {
    this.props.navigation.navigate('DecolagemDetalhes', {
      manifesto_id: id,
      _loadManifestoData: this._loadManifestoData.bind(this)
    });
  }

  excuteTimer(id) {
    // alert(id)
    // this.setState({
    //   endCountdown:true
    // })
  }
  _validaTimer = (data_limit) => {

    var startDate = new Date();
    // Do your operations
    var endDate = new Date(data_limit);

    var seconds = (endDate.getTime() - startDate.getTime()) / 1000;

    if (seconds < 0)
      return false
    else
      return true

  }

  render() {
    const { decolagemList, loadDados, endCountdown } = this.state;
    const tabTitle = this.props.route.params.tabTitle
    console.log(loadDados)
    if (loadDados) {
      console.log(decolagemList)
      if (decolagemList != ""  ) {
        console.log(decolagemList)
        return (

          <ScrollView contentContainerStyle={{ height: '100%', paddingBottom: 80 }}>
            <View>
            <Header titulo={tabTitle}  navigation={this.props.navigation}/>
              <List dataArray={decolagemList}
                renderRow={(list) =>

                  <Card>
                    <View style={{ flexDirection: 'row' }} >
                      <View style={{ flex: 1, flexDirection: 'column', backgroundColor: '#f5f5f5' }} >
                        <View style={{ backgroundColor: '#FEEE00', padding: 5 }} ><Text style={{ color: '#000', fontSize: 15 }} >N˚ Decolagem: {list.decolagem.numero_decolagem}</Text></View>
                        <View style={{ margin: 10 }} >
                          <Text style={{ fontSize: 15, marginVertical: 3 }} >Decolagem Em</Text>

                          <Text style={{ fontSize: 15, marginVertical: 3, height: endCountdown ? 0 : 40 }} >
                            {/* {this._validaTimer(list.decolagem.data_decolagem) ? <CountDownTimer  dateLimit={list.decolagem.data_decolagem}/> : 'Encerrado'} */}
                            <CountDownTimer dateLimit={list.decolagem.data_decolagem} />
                          </Text>
                          <Text style={{ fontSize: 15, marginVertical: 3, height: endCountdown ? 40 : 0 }} >
                            Encerrado
                          </Text>
                          <Text style={{ fontSize: 15, marginVertical: 3 }} >{list.decolagem.aviao_nome}</Text>
                        </View>
                      </View>

                      <View style={{ flex: 1.4, alignItems: 'center', justifyContent: 'center', backgroundColor: '#e8e8e8' }} >
                        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                          <Button onPress={() => { this.detalhesNew(list.decolagem.id) }} style={{
                            backgroundColor: '#FEEE00', width: 200, alignItems: 'center', justifyContent: 'center', borderWidth: 1,
                            borderColor: '#ddd',
                            borderBottomWidth: 0,
                            shadowColor: '#000',
                            shadowOffset: { width: 0, height: 2 },
                            shadowOpacity: 0.8,
                            shadowRadius: 2,
                            elevation: 1,
                          }} rounded >
                            <Text style={{ color: '#000', fontWeight: 'bold' }} >VER DETALHES</Text>
                          </Button>
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                          <View style={{
                            backgroundColor: '#FEEE00', width: 40,
                            height: 40,
                            borderColor: '#000',
                            borderRadius: 100,
                            overflow: 'hidden',
                            borderWidth: 1,
                            alignItems: 'center', justifyContent: 'center',
                          }} >
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }} >{list.decolagem.vagas_restantes}</Text>
                          </View>
                          <View style={{ marginTop: 5, marginLeft: 10 }} ><Text style={{ fontSize: 22 }} >Vagas</Text></View>
                        </View>
                      </View>
                    </View>
                  </Card>
                }>
              </List>

            </View>
          </ScrollView>

        );
      } else {
        return (
          <Container>
            <Content>
            <Header titulo={tabTitle}  navigation={this.props.navigation}/>
              <View style={{ flex: 1, marginTop: 50, alignItems: "center", justifyContent: 'center' }}>
                <View style={{ marginBottom: 50 }} >
                  <Text style={{ fontSize: 20 }} >Não Temos Decolagens Cadastradas!</Text>
                </View>
                <Image source={require('../assets/img/icone-sem-content.png')} />
              </View>
            </Content>
          </Container>
        );
      }
    } else {
      return (
        <Container>
          <Content contentContainerStyle={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
            <ActivityIndicator />
          </Content>
        </Container>
      )
    }
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
