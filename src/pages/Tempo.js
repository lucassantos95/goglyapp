import React, { Component } from 'react';
import {
  Image,
  ActivityIndicator,
  StyleSheet,
  Linking,
  ScrollView,
  TouchableOpacity
} from 'react-native';

import { Container, Content, View, Text, Tab } from 'native-base';
import PureChart from 'react-native-pure-chart';
import api from '../services/Api';
import { converterHora, hosHOSTURL } from '../services/Helpers';
import Header from '../components/HeaderAvatar';

var data = [
  {
    seriesName: 'series2',
    data: [
      { x: '08', y: 10 },
      { x: '09', y: 15 },
      { x: '10', y: 20 },
      { x: '11', y: 25 },
      { x: '12', y: 29 },
      { x: '13', y: 30 },
      { x: '14', y: 28 }

    ]

    , color: 'blue'
  }
]

export default class Tempo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loadData: false,
      horaXtemperatura: [{
        seriesName: 'series2',
        data: [
          // { x: '00', y: 60 },
          // { x: '09', y: 15 },
          // { x: '10', y: 20 },
          // { x: '11', y: 25 },
          // { x: '12', y: 29 },
          // { x: '13', y: 30 },
          // { x: '14', y: 28 }
        ]
        , color: 'blue',
        vazio: true
      }],
      vazio: true,
      dadosClima: null,
      linkYtbe:null
    };

  }


  _loadWatherData = async () => {
    await api.get('/tools');
    const response = await api.get('/weather');

    if (response.data.status == "Success") {
      const response2 = await api.get('/weather/get-settings');
      var linkYoutube = response2.data.settings.link_camera_youtube
      this.setState({
        linkYtbe:linkYoutube
      })
      /* INICIO TRATAMENTO VENTO */
      var hora = 0;
      var tempo = 10;
      var count = 0;

      response.data.weatherDados.vento.forEach(weather => {
        var dados = JSON.parse(weather.info);

        var horario = converterHora(dados.date)
        var graus = dados.temperature.temperature;

        var temperatura = {
          y: graus,
          x: horario + ':00'
        };

        if (count < 24)
          this.state.horaXtemperatura[0].data.push(temperatura);

        count = count + 1;
      });
      /* FIM TRATAMENTO VENTO */


      /* INICIO TRATAMENTO CLIMA */

      if (response.data.weatherDados.clima != '') {
        var clima = JSON.parse(response.data.weatherDados.clima[0].info);

        var dadosClima = {
          temperature: clima.data.temperature,
          condition: clima.data.condition,
          humidity: clima.data.humidity,
          iconeClima: clima.data.icon,
          wind_direction: clima.data.wind_direction,
          wind_velocity: clima.data.wind_velocity
        }
        this.setState({
          dadosClima
        })
      }

      /* FIM TRATAMENTO CLIMA */
    }


    this.setState({
      loadData: true
    })

  }

  componentDidMount() {
    this._loadWatherData();
  }

  switchScreen(index) {
    this.setState({ index: index })
  }

  static navigationOptions = {
    title: 'Lots of features here',
  };

  render() {

    const { horaXtemperatura, vazio, loadData, dadosClima,linkYtbe } = this.state;
    console.log(this.props.route)
    const tabTitle = this.props.route.params.tabTitle
    console.log(dadosClima)

    if (loadData) {
      if (horaXtemperatura[0].data != '') {
        var icone = dadosClima != '' ? dadosClima.iconeClima : '';
        var textPrevisao = dadosClima != '' ? dadosClima.condition : '';
        var temperature = dadosClima != '' ? dadosClima.temperature : '';
        var umidade = dadosClima != '' ? parseInt(dadosClima.humidity) : '';
        var wind_velocity = dadosClima != '' ? dadosClima.wind_velocity : '';
        var wind_direction = dadosClima != '' ? dadosClima.wind_direction : '';
        var wind_direction = 'W';

        console.log(hosHOSTURL + '/icones-tempo/' + dadosClima.iconeClima + '.png')
        return (
          <ScrollView contentContainerStyle={{ height: '100%', paddingBottom: 80 }}>
            <View>
            <Header hasBack titulo={tabTitle}  navigation={this.props.navigation}/>
              <View style={{ flexDirection: 'row', margin: 5 }} >
                <View style={{ flex: 1, marginRight: 5, justifyContent: 'center', alignItems: 'center', }}>

                  <Image
                    style={{ width: 130, height: 100 }}
                    source={{ uri: hosHOSTURL + '/icones-tempo/' + icone + '.png' }}
                  />
                </View>

                <View style={{ flex: 1, marginLeft: 5, padding: 10 }} >
                  <Text style={{ fontSize: 80, textAlign: 'right', left: 10 }} >{temperature}˚</Text>
                  <Text style={{ textAlign: 'right' }} >Boituva - SP</Text>
                  <Text style={{ textAlign: 'right' }}  >{textPrevisao}</Text>
                </View>
              </View>

              <View style={{ padding: 0, marginRight: 0 }} >


                <PureChart type={'line'}
                  data={this.state.horaXtemperatura}
                  width={'100%'}
                  height={200}
                  style={{}}
                  onPress={(a) => {
                    console.log('onPress', a)
                  }}
                  xAxisColor={'#f2f2f2'}
                  yAxisColor={'#f2f2f2'}
                  xAxisGridLineColor={'#f2f2f2'}
                  yAxisGridLineColor={'#f2f2f2'}

                  labelColor={'black'}
                  showEvenNumberXaxisLabel={false}
                  customValueRenderer={(index, point) => {

                    return (
                      <Text style={{ textAlign: 'center', fontSize: 12 }}>{point.y + '˚'}</Text>
                    )
                  }}
                />
              </View>

              <View style={{ flexDirection: 'row', margin: 10 }} >
                <View style={{ flex: 1, backgroundColor: '#EEE', paddingVertical: 10, marginHorizontal: 5, alignContent: 'center', alignItems: 'center' }} >
                  <Text>Umidade</Text>
                  <View style={{ marginTop: 5 }} >
                    <Text style={{ fontSize: 35 }} >{umidade}%</Text>
                  </View>
                </View>
                <View style={{ flex: 1, backgroundColor: '#EEE', paddingVertical: 10, marginHorizontal: 5, alignContent: 'center', alignItems: 'center' }} >
                  <Text>Câmera ao Vivo</Text>
                  <TouchableOpacity
                    style={{ marginTop: 5 }}
                    onPress={() => { Linking.openURL(linkYtbe) }}
                  >
                    <Image
                      style={{ width: 40, height: 40 }}
                      source={require('../assets/img/icone-camera.png')}
                    />
                  </TouchableOpacity>
                </View>
              </View>

              <View style={{ flexDirection: 'row', margin: 10 }} >

                <View style={{ flex: 1, backgroundColor: '#EEE', paddingVertical: 10, marginHorizontal: 5, alignContent: 'center', alignItems: 'center' }} >
                  <Text>Direção do Vento</Text>
                  <View style={{ marginTop: 5 }} >
                    <Text style={{ fontSize: 35 }} >{wind_direction}</Text>
                  </View>
                </View>

                <View style={{ flex: 1, backgroundColor: '#EEE', paddingVertical: 10, marginHorizontal: 5, alignContent: 'center', alignItems: 'center' }} >
                  <Text>Velocidade do Vento</Text>
                  <View style={{ marginTop: 5, flexDirection:'row' }} >
                    <View>
                    <Text style={{ fontSize: 35 }} >{wind_velocity} </Text>
                    </View>
                    <View style={{marginTop:18}} >
                    <Text style={{fontSize:15}}>km/h</Text>
                    </View>
                  </View>
                </View>

              </View>

            </View>
          </ScrollView>
        );
      } else {
        return (
          <Container>
            <Content>
            <Header titulo={tabTitle}  navigation={this.props.navigation}/>
              <View style={{ flex: 1, marginTop: 50, alignItems: "center", justifyContent: 'center' }}>
                <View style={{ marginBottom: 50 }} >
                  <Text style={{ fontSize: 20 }} >Estamos Sem Previsão do Tempo!</Text>
                </View>
                <Image source={require('../assets/img/icone-sem-content.png')} />
              </View>
            </Content>
          </Container>
        );

      }
    } else {
      return (
        <Container>
          <Content contentContainerStyle={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
            <ActivityIndicator />
          </Content>
        </Container>
      )
    }

  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
