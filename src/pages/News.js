import React, { Component } from 'react';
import {
  Dimensions,
  StyleSheet,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView,
} from 'react-native';

import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  Container,
  Content,
  List,
  View,
  Card,
  CardItem,
  Text,
  Left,
  Body,
  Thumbnail,
  Right,
  Button,
} from 'native-base';

import api from '../services/Api';

import { hosHOSTURL } from '../services/Helpers';
import Header from '../components/HeaderAvatar';
export default class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blogList: '',
      loadDados: false,
    };
  }

  _loadNewsData = async () => {
    const response = await api.get('/news');
    if (response.data.status == 'Success') {
      this.setState({
        blogList: response.data.news,
      });
    }
    this.setState({
      loadDados: true,
    });
  };

  componentDidMount() {
    this._loadNewsData();
  }

  detalhesNew = (id) => {
    this.props.navigation.navigate('NewsDetalhes', {
      news_id: id,
    });
  };
  render() {
    const { height: screenHeight } = Dimensions.get('window');
    const { blogList, loadDados } = this.state;
    console.log('blogList');
    console.log(this.props.route)
    const tabTitle = this.props.route.params?.tabTitle ?? 'News'

    if (loadDados) {
      if (typeof blogList != 'undefined' && blogList != '') {
        return (
          <ScrollView
            contentContainerStyle={{ height: '100%', paddingBottom: 80 }}>
            <View>
              <Header hasBack titulo={tabTitle} navigation={this.props.navigation} />
              {blogList.map(list => {
                return  (
                  <TouchableOpacity
                  key={list.id}
                    onPress={() => {
                      this.detalhesNew(list.id);
                    }}>
                    <Card>
                      <CardItem style={{ flexBasis: 0 }}>
                        <View>
                          <Thumbnail
                            style={{ width: 100, height: 100 }}
                            square
                            source={{ uri: hosHOSTURL + '/news/' + list.foto }}
                          />
                        </View>
                        <Body>
                          <View
                            style={{
                              flexDirection: 'column',
                              height: 100,
                              paddingLeft: 15,
                            }}>
                            <View style={{ marginBottom: 10 }}>
                              <Text>{list.titulo}</Text>
                            </View>
                            <View style={{ flex: 2 }}>
                              <Text note numberOfLines={3}>
                                {list.texto}{' '}
                              </Text>
                            </View>
                          </View>
                        </Body>
                      </CardItem>
                    </Card>
                  </TouchableOpacity>
                )
              })}
           
            </View>
          </ScrollView>
        );
      } else {
        return (
          <Container>
            <Content>
              <Header titulo={tabTitle} navigation={this.props.navigation} />
              <View
                style={{
                  flex: 1,
                  marginTop: 50,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <View style={{ marginBottom: 50 }}>
                  <Text style={{ fontSize: 20 }}>
                    Não Temos News Cadastradas!
                  </Text>
                </View>
                <Image
                  source={require('../assets/img/icone-sem-content.png')}
                />
              </View>
            </Content>
          </Container>
        );
      }
    } else {
      return (
        <Container>
          <Content
            contentContainerStyle={{
              alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
            }}>
            <ActivityIndicator />
          </Content>
        </Container>
      );
    }
  }

  _showMoreApp = () => {
    this.props.navigation.navigate('Other');
  };

  _signOutAsync = async () => {
    console.log(this.props.navigation);
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
