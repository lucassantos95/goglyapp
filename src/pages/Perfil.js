import { Container, Content, Input, Item, Text, View, Icon, Button } from 'native-base';
import React, { Component } from 'react';
import { StyleSheet, Alert, Image } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Header from '../components/HeaderSemAvatar';
import Icon2 from 'react-native-vector-icons/Entypo';
import { useNavigation, StackActions } from '@react-navigation/native';
// import the component
import { TextInputMask } from 'react-native-masked-text'
import { hosHOSTURL } from '../services/Helpers';
import { converterDatas, validaCpf, onlyNumber } from '../services/Helpers';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from 'moment'
import api from '../services/Api';


export default class Cadastro extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isDateTimePickerVisible: false,
            nome: '',
            apelido: '',
            data_nasc: '',
            cpf: '',
            email: '',
            senha: '',
            fotoPerfil: '',
            loadDados: false
        };

    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {
        this._loadPerfilData();
    }

    pickFromCamera = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
                return Alert.alert(
                    'Go Fly',
                    'Vá até as configurações do android e ative a permissão para o App acessar a camera!',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                )
        }
        let image = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: false,
            aspect: [3, 4],
            base64: true,
        }).catch((error) => {
            console.log(error)
            // alert(error)
            if (error == 'Error: User rejected permissions') {
                return Alert.alert(
                    'Go Fly',
                    'Vá até as configurações do android e ative a permissão para o App acessar a camera!',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ],
                )
            }
        });

        if (typeof image.base64 != 'undefined') {

            const usuario_dados = await AsyncStorage.getItem('Usuario');
            var usuario = JSON.parse(usuario_dados);

            const userTipo = await AsyncStorage.getItem('Usuario.tipo');
            console.log(userTipo)
            var dados = { usuario_id: usuario.id, userTipo: userTipo, foto_perfil: image.base64};

            const response = await api.post('/login/update-foto-perfil', dados);
            console.log(response)

            this.setState({
                fotoPerfil: `data:image/gif;base64,${image.base64}`
            });
        }

    }

    _loadPerfilData = async () => {

        const usuario_dados = await AsyncStorage.getItem('Usuario');
        var usuario = JSON.parse(usuario_dados);

        const userTipo = await AsyncStorage.getItem('Usuario.tipo');
        console.log(userTipo)
        var dados = { params:{usuario_id: usuario.id, userTipo: userTipo }};

        const response = await api.get('/login/get-users-dados', dados);
        console.log(response)
        if (response.data.status == "Success") {
            console.log(response.data.usuario)

            var usuario = response.data.usuario;

            this.setState({
                nome: usuario.nome,
                apelido: usuario.apelido,
                data_nasc: (usuario.data_nascimento) ? converterDatas(usuario.data_nascimento) : usuario.data_nascimento,
                cpf: usuario.cpf,
                email: usuario.email,
                fotoPerfil: (usuario.foto_perfil == null) ? 'https://cdn.iconscout.com/icon/free/png-256/account-profile-avatar-man-circle-round-user-30452.png' : `${hosHOSTURL}foto_perfil/${usuario.foto_perfil}`
            })
        }
        this.setState({
            loadDados: true
        })

    }
    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        
        var data_niver = moment(date).format('DD-MM-YYYY')
        console.log(data_niver)
        this.setState({
            data_nasc: data_niver
        })
        this._hideDateTimePicker();
    };

    logout = () => {

        return Alert.alert(
            'Go Fly',
            'Você deseja realmente sair ?',
            [
                {
                    text: 'Não',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: 'Sim', onPress: async () => {
                        await AsyncStorage.clear();
                        return this.props.navigation.dispatch(StackActions.replace('Auth'));
                    }
                },
            ],
            { cancelable: false },
        );
    }

    salvarDados = async () => {
        var { nome, data_nasc, cpf, email, senha, apelido } = this.state;

        if (nome == '')
            return Alert.alert(
                'Go Fly',
                'Preencher o campo nome',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
            )

        if (data_nasc == '')
            return Alert.alert(
                'Go Fly',
                'Preencher o campo Data de Nascimento',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
            )

        if (cpf == '')
            return Alert.alert(
                'Go Fly',
                'Preencher o campo Data de CPF',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
            )


        if (email == '')
            return Alert.alert(
                'Go Fly',
                'Preencher o campo Data de E-mail',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
            )

        var data_nasc = converterDatas(data_nasc);
        var isCpf = validaCpf(cpf);

        if (data_nasc == false) {
            return alert('Digite uma Data Válida!');
        }

        if (isCpf == false) {
            return alert('Digite um cpf válido !');
        } else {
            cpf = cpf;
        }

        const userTipo = await AsyncStorage.getItem('Usuario.tipo');
        const usuario_dados = await AsyncStorage.getItem('Usuario');
        var usuario = JSON.parse(usuario_dados);


        var dados = {
            nome, data_nasc, cpf, email, apelido, userTipo, usuario_id: usuario.id,

        }

        if (senha != '')
            dados['senha'] = senha;

        const response = await api.post('/login/update-user', dados);
        console.log(response);
        console.log(response.data.id);
        if (response.data.status == 'Success') {
            return Alert.alert(
                'Go Fly',
                'Dados alterados com sucesso!',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
            )
        } else {
            return Alert.alert(
                'Go Fly',
                'Erro ao tentar alterar os dados, Tente novamente!',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
            )
        }
    }
    //prefix base64 data:image/gif;base64,
    render() {
        console.log(this.state.fotoPerfil)
        return (
            <Container>
                <Header titulo={'Meus Dados'} navigation={this.props.navigation} />
                <Content>
                    <View style={styles.containerMain} >

                        <View style={{ flexDirection: 'column' ,justifyContent: 'center',  alignItems: 'center',}}>
                            <View style={{ width: 200, height: 200, borderWidth: 1, marginVertical: 10, padding: 5 }}>
                                <Image style={{ flex: 1 }} source={{ uri: this.state.fotoPerfil }} />
                            </View>
                            <View style={{ marginVertical: 10 }}>
                                <Button onPress={this.pickFromCamera} info><Text> Alterar Foto </Text></Button>
                            </View>
                        </View>

                        <View style={styles.containerRow} >
                            <Text>NOME *</Text>
                            <Text style={{ fontSize: 12 }} >(*) Campos Obrigatorios</Text>
                        </View>
                        <Item regular>
                            <Input
                                onChangeText={(nome) => this.setState({ nome })}
                                value={this.state.nome}
                            />
                        </Item>
                        <Text style={styles.label} >Apelido</Text>
                        <Item regular>
                            <Input
                                onChangeText={(apelido) => this.setState({ apelido })}
                                value={this.state.apelido}
                            />
                        </Item>
                        <Text style={styles.label} >DATA DE NASCIMENTO *</Text>
                        <Item regular>

                            <TextInputMask
                                refInput={(ref) => this.myDateText = ref}
                                style={styles.inputTeste}
                                type={'datetime'}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                options={{
                                    format: 'DD-MM-YYYY'
                                }}
                                onTouchStart={this._showDateTimePicker}
                                maxLength={10}
                                value={this.state.data_nasc}
                                onChangeText={data_nasc => {
                                    this.setState({ data_nasc })
                                }}
                            />
                        </Item>
                        <Text style={styles.label} >CPF *</Text>
                        <Item regular>
                            <TextInputMask
                                refInput={(ref) => this.myCpfText = ref}
                                style={styles.inputTeste}
                                type={'cpf'}
                                underlineColorAndroid='rgba(0,0,0,0)'
                                options={{
                                    format: '999.999.999-99'
                                }}
                                maxLength={14}
                                value={this.state.cpf}
                                onChangeText={cpf => {
                                    this.setState({ cpf })
                                }}
                            />
                        </Item>
                        <Text style={styles.label} >E-MAIL</Text>
                        <Item regular>
                            <Input
                                onChangeText={(email) => this.setState({ email })}
                                value={this.state.email}
                                keyboardType={'email-address'}
                            />
                        </Item>

                        <Text style={styles.label} >SENHA</Text>
                        <Item regular>
                            <Input
                                onChangeText={(senha) => this.setState({ senha })}
                                value={this.state.senha}
                                textContentType={'password'}
                                secureTextEntry={true}
                            />
                        </Item>


                        <View style={{ flexDirection: 'row', justifyContent: "space-between", marginVertical: 10 }} >
                            <Button style={{ backgroundColor: '#efefef' }} iconRight light onPress={this.salvarDados}  >
                                <Text style={{ paddingRight: 0, marginRight: 5 }} >Salvar</Text>
                                <Icon2 name='save' style={{ fontSize: 25, paddingRight: 10, paddingLeft: 0 }} />
                            </Button>
                            <Button style={{ backgroundColor: '#efefef' }} iconRight light onPress={this.logout}>
                                <Text>Logout</Text>
                                <Icon name='md-exit' />
                            </Button>
                        </View>

                        <DateTimePicker
                            isVisible={this.state.isDateTimePickerVisible}
                            onConfirm={this._handleDatePicked}
                            onCancel={this._hideDateTimePicker}
                            cancelTextIOS={'Cancelar'}
                            confirmTextIOS={'Confirmar'}
                            titleIOS={'Data de Nascimento'}
                        // datePickerModeAndroid={'spinner'}
                        />

                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    containerMain: {
        padding: 25,
        flex: 1
    },
    containerRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    containerRow2: {
        flexDirection: 'row',
        marginVertical: 10
    },
    label: {
        marginVertical: 10
    },

    inputTeste: {
        height: 50,
        width: '100%',
        padding: 10,
        fontSize: 17
    }


});