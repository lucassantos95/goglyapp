import React, { Component } from 'react';
import {
  StatusBar,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  View,
  Body,
  Text,
} from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import api from '../services/Api';

export default class SaldoSaltos extends Component {
  constructor() {
    super();
    this.state = {
      saldo: [0, 0],
      loadDados: false,
    };
  }

  _loadNewsData = async () => {
    const atleta_dados = await AsyncStorage.getItem('Usuario');
    var atleta_id = JSON.parse(atleta_dados);

    var dados = { params:{atleta_id: atleta_id.id }};

    const response = await api.get('/atletas-saldo/index', dados);
    if (response.data.status == 'Success') {
      var saldo = parseInt(response.data.saldo.saldo).toString();

      if (saldo != '' || typeof saldo != 'undefined') {
        if (saldo.length == 1) {
          saldo = '0' + saldo;
        }
        this.setState({ saldo: saldo.split('') });
      } else {
        saldo = [0, 0];
        this.setState({ saldo });
      }
    } else {
      var numero = '00';
      this.setState({ saldo: numero.split('') });
    }
    this.setState({
      loadDados: true,
    });
  };

  componentDidMount() {
    this._loadNewsData();
  }

  static navigationOptions = {
    title: 'Lots of features here',
  };

  render() {
    const { saldo, loadDados } = this.state;
    console.log(saldo);
    if (loadDados) {
      return (
        <Container>
          <Content
            contentContainerStyle={{
              alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
            }}>
            <Grid>
              <Row
                style={{
                  alignItems: 'flex-end',
                  justifyContent: 'center',
                }}>
                <View style={{ margin: 15 }}>
                  <Text style={{ fontSize: 20 }}>O Seu Saldo Atual é de</Text>
                </View>
              </Row>
              <Row
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  flex: 2,
                }}>
                {saldo.map((saldo, index) => {
                  return (
                    <Col
                      key={index}
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: '#000',
                        width: 100,
                        marginRight: 5,
                        height: 200,
                      }}>
                      <View key={index}>
                        <Text
                          key={index}
                          style={{
                            color: '#FEEE00',
                            fontSize: 100,
                            fontWeight: 'bold',
                          }}>
                          {saldo}
                        </Text>
                      </View>
                    </Col>
                  );
                })}
              </Row>
              <Row
                style={{
                  flex: 2,
                  justifyContent: 'center',
                }}>
                <View style={{ margin: 15 }}>
                  <Text style={{ fontSize: 20 }}>Saltos. Go for It!</Text>
                </View>
              </Row>
            </Grid>
          </Content>
        </Container>
      );
    } else {
      return (
        <Container>
          <Content
            contentContainerStyle={{
              alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
            }}>
            <ActivityIndicator />
          </Content>
        </Container>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
