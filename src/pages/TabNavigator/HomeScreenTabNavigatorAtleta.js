import React, { Component } from 'react';

import { createBottomTabNavigator } from 'react-navigation';

import News from '../News';
import Tempo from '../Tempo';
import Decolagem from '../Decolagem';
import Saltos from '../Saltos';
import Store from '../Store';
import { Icon } from 'native-base';
import Icon2 from 'react-native-vector-icons/Entypo';

import { createIconSetFromFontello } from 'react-native-vector-icons';
import fontelloConfig from '../../assets/fonts/config.json';

const AtletaSalto = createIconSetFromFontello(fontelloConfig);

const HomeScreenTabNavigatorAtleta = createBottomTabNavigator({
    News: {
        screen: News,
        navigationOptions: {
            tabBarLabel: 'News',
            tabBarIcon: () => (<Icon name="book" size={30} />)
        }
    },
    Tempo: {
        screen: Tempo,
        navigationOptions: {
            tabBarLabel: 'Tempo',
            tabBarIcon: () => (<Icon name="md-partly-sunny" size={24} />)
        }
    },
    Decolagem: {
        screen: Decolagem,
        navigationOptions: {
            tabBarLabel: 'Decolagem',
            tabBarIcon: () => (<Icon name="md-paper-plane" size={24} />)
        }
    },
    Saltos: {
        screen: Saltos,
        navigationOptions: {
            tabBarLabel: 'Saltos',
            tabBarIcon: () => (<AtletaSalto />)
        }
    },
    Store: {
        screen: Store,
        navigationOptions: {
            tabBarLabel: 'Store',
            tabBarIcon: () => (<Icon name="md-pricetags" size={24} />)
        }
    },
});


export default HomeScreenTabNavigatorAtleta;