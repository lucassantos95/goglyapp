import React, { Component } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Container, Content} from 'native-base';

import Tabbar from 'react-native-tabbar-bottom';
import Header from '../../components/HeaderAvatar';

import News from '../News';
import Tempo from '../Tempo';
import Decolagem from '../Decolagem';
import Saltos from '../Saltos';
import Store from '../Store';


import Agenda from '../Agenda';
import Midia from '../Midia';

export default class HomeScreenTabNavigator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            tipoUser: false,
            page: 'News'
        };

        this._getUserTipo();
    }

    switchScreen(index) {
        this.setState({ index: index })
    }

    static navigationOptions = {
        header: null
    };

    _getUserTipo = async () => {
        const userTipo = await AsyncStorage.getItem('Usuario.tipo');
        console.log(userTipo)
        this.setState({
            tipoUser: userTipo
        })
    }

    render() {
        if (!this.state.tipoUser) {
            return null;
        }
        else {

            let AppComponent = null;

            if (this.state.page == "News") {
                AppComponent = News
            } else if (this.state.page == "Tempo") {
                AppComponent = Tempo
            } else if (this.state.page == "Decolagem") {
                AppComponent = Decolagem
            } else if (this.state.page == "Saltos") {
                AppComponent = Saltos
            } else if (this.state.page == "Store") {
                AppComponent = Store
            }else if (this.state.page == "Midia") {
                AppComponent = Midia
            }else if (this.state.page == "Agenda") {
                AppComponent = Agenda
            }

            var TabBarCondition = null;


            if (this.state.tipoUser == 'cliente') {
                console.log('user 1')
                TabBarCondition = [
                    {
                        iconText: "News",
                        page: "News",
                        icon: "md-home",
                    },
                    {
                        page: "Tempo",
                        iconText: "Tempo",
                        icon: "md-partly-sunny",
                        // badgeNumber: 9,
                    },
                    {
                        page: "Midia",
                        iconText: "Midia",
                        icon: "md-film",
                    },
                    {
                        page: "Agenda",
                        iconText: "Agenda",
                        icon: "md-calendar",
                        // badgeNumber: 7,
                    },
                    {
                        page: "Store",
                        iconText: "Store",
                        icon: "md-pricetags",
                    },
                ];
            } else if (this.state.tipoUser == 'atleta') {
                console.log('user 2')
                TabBarCondition = [
                    {
                        iconText: "News",
                        page: "News",
                        icon: "md-home",
                    },
                    {
                        page: "Tempo",
                        iconText: "Tempo",
                        icon: "md-partly-sunny",
                        // badgeNumber: 9,
                    },
                    {
                        page: "Decolagem",
                        iconText: "Decolagem",
                        icon: "md-paper-plane",
                    },
                    {
                        page: "Saltos",
                        iconText: "Saltos",
                        icon: "md-man",
                        // badgeNumber: 7,
                    },
                    {
                        page: "Store",
                        iconText: "Store",
                        icon: "md-pricetags",
                    },
                ];
            }


            return (

                <Container>
                    {/* <Header titulo={this.state.page}  navigation={this.props.navigation}/> */}
                    <Content>
                        <AppComponent navigation={this.props.navigation} route={this.props.route} />
                    </Content>
                    <Tabbar
                        type="ripple"
                        rippleColor="#1F1F1F"
                        tabbarBgColor="#1F1F1F"
                        iconColor="#fff"
                        selectedIconColor="#FEEE00"
                        labelColor="#fff"
                        selectedLabelColor="#FEEE00"

                        stateFunc={(tab) => {
                            this.setState({ page: tab.page })
                            this.props.navigation.setParams({tabTitle: tab.page})
                        }}
                        activePage={this.state.page}
                        tabs={TabBarCondition}
                    />
                </Container>
            );
        }
    }
}
