import React, { Component } from 'react';

import { Icon } from 'native-base';
import { createBottomTabNavigator } from 'react-navigation';

import News from '../News';
import Tempo from '../Tempo';
import Store from '../Store';
import Agenda from '../Agenda';
import Midia from '../Midia';


const HomeScreenTabNavigatorCliente = createBottomTabNavigator({
    News: {
        screen: News,
        navigationOptions: {
            tabBarLabel: 'News',
            tabBarIcon: () => (<Icon name="book" size={24} />)
        }
    },
    Tempo: {
        screen: Tempo,
        navigationOptions: {
            tabBarLabel: 'Tempo',
            tabBarIcon: () => (<Icon name="md-partly-sunny" size={24} />)
        }
    },
    Midia: {
        screen: Midia,
        navigationOptions: {
            tabBarLabel: 'Midia',
            tabBarIcon: () => (<Icon name="md-film" size={24} />)
        }
    },
    Agenda: {
        screen: Agenda,
        navigationOptions: {
            tabBarLabel: 'Agenda',
            tabBarIcon: () => (<Icon name="md-calendar" size={24} />)
        }
    },
    Store: {
        screen: Store,
        navigationOptions: {
            tabBarLabel: 'Store',
            tabBarIcon: () => (<Icon name="md-compass" size={24} />)
        }
    },
});

export default HomeScreenTabNavigatorCliente;
