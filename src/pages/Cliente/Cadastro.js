import {
  Container,
  Content,
  Input,
  Item,
  Text,
  View,
  Icon,
  Button,
  ListItem,
  Body,
  CheckBox,
} from 'native-base';
import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Alert,
  Keyboard,
  Platform,
  KeyboardAvoidingView,
} from 'react-native';
import Header from '../../components/HeaderSemAvatar';

// import the component
import { TextInputMask } from 'react-native-masked-text';

import { converterDatas, validaCpf, onlyNumber } from '../../services/Helpers';

import DateTimePicker from 'react-native-modal-datetime-picker';

import api from '../../services/Api';
import Loading from 'react-native-whc-loading';

export default class Cadastro extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDateTimePickerVisible: false,
      nome: '',
      data_nasc: '',
      cpf: '',
      email: '',
      celular: '',
      apelido: '',
      user: '',
      senha: '',
      confirmar_senha: '',
      checkTermos: false,
      passaporte: '',
      keyboardAvoidingViewKey: 'keyboardAvoidingViewKey',
    };
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    console.log(date);
    var data_niver = new Date(date);

    (dia = data_niver.getDate().toString()),
      (diaF = dia.length == 1 ? '0' + dia : dia),
      (mes = (data_niver.getMonth() + 1).toString()),
      (mesF = mes.length == 1 ? '0' + mes : mes),
      (anoF = data_niver.getFullYear());

    var data_fim = diaF + '-' + mesF + '-' + anoF;
    // date = date.toString();
    // var dataTrata =  date.split('T');
    console.log(data_fim);
    this.setState({
      data_nasc: data_fim,
    });
    this._hideDateTimePicker();
  };

  componentDidMount() {
    // using keyboardWillHide is better but it does not work for android
    this.keyboardHideListener = Keyboard.addListener(
      Platform.OS === 'android' ? 'keyboardDidHide' : 'keyboardWillHide',
      this.keyboardHideListener.bind(this),
    );
  }

  componentDidMount() {
    const nome = this.props.route.params?.nome;
    const email = this.props.route.params?.email;
    const user = this.props.route.params?.user;
    console.log(this.props);
    this.setState({
      nome,
      email,
      user,
    });
  }

  keyboardHideListener() {
    this.setState({
      keyboardAvoidingViewKey: 'keyboardAvoidingViewKey' + new Date().getTime(),
    });
  }

  static navigationOptions = {
    header: null,
  };

  cadastrar = async () => {
    try {
      // this.refs.loading4.show();
      var {
        nome,
        data_nasc,
        cpf,
        email,
        celular,
        apelido,
        senha,
        confirmar_senha,
        checkTermos,
        passaporte,
        user
      } = this.state;

      if (nome == '') {
        this.refs.loading4.close();

        return Alert.alert('Go Fly', 'Preencher o campo nome', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }

      if (data_nasc == '' && Platform.OS == 'android') {
        this.refs.loading4.close();
        return Alert.alert('Go Fly', 'Preencher o campo Data de Nascimento', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }
      if (cpf == '' && passaporte == '' && Platform.OS == 'android') {
        this.refs.loading4.close();
        return Alert.alert('Go Fly', 'Preencher o campo de CPF ou PASSAPORTE', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }
      if (email == '') {
        return Alert.alert('Go Fly', 'Preencher o campo de E-mail', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }
      if (celular == '' && Platform.OS == 'android') {
        this.refs.loading4.close();
        return Alert.alert('Go Fly', 'Preencher o campo de Celular', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }

      if (senha == '') {
        this.refs.loading4.close();
        return Alert.alert('Go Fly', 'Preencher o campo de Senha', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }

      if (confirmar_senha == '') {
        this.refs.loading4.close();
        return Alert.alert('Go Fly', 'Preencher o campo de Confirmar Senha', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }

      if (cpf != '') {
        var isCpf = validaCpf(cpf);
        if (isCpf == false) {
          this.refs.loading4.close();
          return alert('Digite um cpf válido !');
        } else {
          cpf = cpf;
        }
      }

      var data_nasc = converterDatas(data_nasc);

      if (data_nasc == false) {
        this.refs.loading4.close();
        return alert('Digite uma Data Válida!');
      }

      if (senha != confirmar_senha) {
        this.refs.loading4.close();
        return alert('As Senhas precisam ser iguais!');
      }

      if (checkTermos == '') {
        this.refs.loading4.close();

        return Alert.alert('Go Fly', 'Faça um Check nos termos de contrato', [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }
      var dados = {
        nome,
        data_nasc,
        cpf,
        email,
        celular,
        apelido,
        senha,
        passaporte,
      };

      if (user) {
        dados.user = user;
      }

      const response = await api.post('/clientes/cadastro', dados);
      this.refs.loading4.close();
      if (response.data.error == 'Success') {
        this.props.navigation.push('FinalizacaoCadastro', {
          email,
          tipoAtleta: 'cliente',
        });
      } else {
        return Alert.alert('Go Fly', response.data.msg, [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]);
      }
    } catch (error) {
      console.log(error)
      this.refs.loading4.close();
      return Alert.alert('Go Fly', 'Desculpe, por favor tente mais tarde!', [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ]);
    }
  };

  checkTermos = () => {
    this.setState({
      checkTermos: !this.state.checkTermos,
    });
  };
  finalizarCadastro = () => {
    this.props.navigation.push('FinalizacaoCadastro');
  };

  openCadastro2 = () => {
    this.props.navigation.navigate('CadastroAtleta2');
  };
  openTermoUso = () => {
    this.props.navigation.push('TermoDeUso');
  };
  render() {
    let { keyboardAvoidingViewKey } = this.state;
    var behavior = Platform.OS === 'android' ? 'padding' : 'height';
    return (
      <Container>
        <Header titulo={'Cadastre-se'} navigation={this.props.navigation} />
        <Content style={{ paddingBottom: 40 }}>
          {/* <KeyboardAvoidingView behavior="height" key={keyboardAvoidingViewKey} style={styles.form}> */}
          <KeyboardAvoidingView
            behavior={behavior}
            key={keyboardAvoidingViewKey}
            style={styles.form}>
            <View style={[styles.containerMain, { paddingBottom: 40 }]}>
              <View style={styles.containerRow}>
                <Text>NOME COMPLETO *</Text>
                <Text style={{ fontSize: 12 }}>(*) Campos Obrigatorios</Text>
              </View>
              <Item regular>
                <Input
                  onChangeText={(nome) => this.setState({ nome })}
                  value={this.state.nome}
                />
              </Item>

              <Text style={styles.label}>APELIDO</Text>
              <Item regular>
                <Input
                  onChangeText={(apelido) => this.setState({ apelido })}
                  value={this.state.apelido}
                />
              </Item>

              <Text style={styles.label}>
                DATA DE NASCIMENTO {Platform.OS == 'android' ? '*' : ''}{' '}
              </Text>
              <Item regular>
                <TextInputMask
                  refInput={(ref) => (this.myDateText = ref)}
                  style={styles.inputTeste}
                  type={'datetime'}
                  underlineColorAndroid="rgba(0,0,0,0)"
                  options={{
                    format: 'DD-MM-YYYY',
                  }}
                  // onTouchStart={this._showDateTimePicker}
                  maxLength={10}
                  value={this.state.data_nasc}
                  onChangeText={(data_nasc) => {
                    this.setState({ data_nasc });
                  }}
                />
              </Item>

              <Text style={styles.label}>CPF </Text>
              <Item regular>
                <TextInputMask
                  refInput={(ref) => (this.myCpfText = ref)}
                  style={styles.inputTeste}
                  type={'cpf'}
                  underlineColorAndroid="rgba(0,0,0,0)"
                  options={{
                    format: '999.999.999-99',
                  }}
                  maxLength={14}
                  value={this.state.cpf}
                  onChangeText={(cpf) => {
                    this.setState({ cpf });
                  }}
                />
              </Item>
              <Text style={styles.label}>PASSAPORTE</Text>
              <Item regular>
                <Input
                  onChangeText={(passaporte) => this.setState({ passaporte })}
                  value={this.state.passaporte}
                />
              </Item>
              <Text style={styles.label}>E-MAIL *</Text>
              <Item regular>
                <Input
                  onChangeText={(email) => this.setState({ email })}
                  value={this.state.email}
                  autoCapitalize="none"
                  keyboardType={'email-address'}
                />
              </Item>
              <Text style={styles.label}>
                CELULAR {Platform.OS == 'android' ? '*' : ''}
              </Text>
              <Item regular>
                <TextInputMask
                  refInput={(ref) => (this.myCelText = ref)}
                  style={styles.inputTeste}
                  type={'cel-phone'}
                  underlineColorAndroid="rgba(0,0,0,0)"
                  options={{
                    format: '(99) 99999-9999',
                  }}
                  maxLength={15}
                  value={this.state.celular}
                  onChangeText={(celular) => {
                    this.setState({ celular });
                  }}
                />
              </Item>
              <Text style={styles.label}>SENHA</Text>
              <Item regular>
                <Input
                  onChangeText={(senha) => this.setState({ senha })}
                  value={this.state.senha}
                  textContentType={'password'}
                  secureTextEntry={true}
                />
              </Item>
              <Text style={styles.label}>CONFIRMAR SENHA</Text>
              <Item regular>
                <Input
                  onChangeText={(confirmar_senha) =>
                    this.setState({ confirmar_senha })
                  }
                  value={this.state.confirmar_senha}
                  textContentType={'password'}
                  secureTextEntry={true}
                />
              </Item>
              <ListItem>
                <CheckBox
                  onPress={this.checkTermos}
                  checked={this.state.checkTermos}
                />
                <Body>
                  <TouchableOpacity onPress={this.openTermoUso}>
                    <Text style={{ textDecorationLine: 'underline' }}>
                      Li e aceitos os termos de uso
                    </Text>
                  </TouchableOpacity>
                </Body>
              </ListItem>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginVertical: 10,
                }}>
                <Button
                  onPress={this.cadastrar}
                  style={{ backgroundColor: '#FEEE00' }}
                  large>
                  <Text style={{ fontWeight: 'bold', color: '#000000' }}>
                    Cadastrar
                  </Text>
                </Button>
              </View>
              <Loading
                ref="loading4"
                backgroundColor={'#00000096'}
                indicatorColor={'#fff'}
              />
              {/* <DateTimePicker
                isVisible={this.state.isDateTimePickerVisible}
                onConfirm={this._handleDatePicked}
                onCancel={this._hideDateTimePicker}
                cancelTextIOS={'Cancelar'}
                confirmTextIOS={'Confirmar'}
                titleIOS={'Data de Nascimento'}
              // datePickerModeAndroid={'spinner'}
              /> */}
            </View>
          </KeyboardAvoidingView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  containerMain: {
    padding: 25,
    flex: 1,
  },
  containerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  containerRow2: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  label: {
    marginVertical: 10,
  },
  inputTeste: {
    height: 50,
    width: '100%',
    padding: 10,
    fontSize: 17,
  },
  form: {
    flex: 1,
  },
});
