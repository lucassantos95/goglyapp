import React, { Component } from 'react';
import { Image, TouchableOpacity, ActivityIndicator, Linking, Share, ScrollView } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { Button, Container, Icon, Text, View, Content } from 'native-base';

import { sliderWidth, itemWidth } from '../components/Carousel/Styles/SliderEntry.style';
import styles, { colors } from '../components/Carousel/Styles/index.style';
import SliderEntry from '../components/Carousel/SliderEntry';
import api from '../services/Api';
import Header from '../components/HeaderAvatar';

import { converterMomento } from '../services/Helpers';

export default class Midia extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loadDados: false,
      linkFotos: false,
      loginConvidado: false
    }
  }

  static navigationOptions = {
    title: 'Lots of features here',
  };

  _loadData = async () => {

    const cliente_dados = await AsyncStorage.getItem('Usuario');
    console.log(cliente_dados)
    if (cliente_dados != null) {
      var cliente_id = JSON.parse(cliente_dados);
      // console.log(cliente_id)
      var dados = { params:{cliente_id: cliente_id.id }};

      const response = await api.get('/midias', dados);

      console.log(response)
      if (response.data.status == "Success") {
        this.setState({
          linkFotos: response.data.midias
        })
      }
    } else {
      this.setState({ loginConvidado: true })
    }
    this.setState({
      loadDados: true
    })

  }

  componentDidMount() {
    this._loadData();
  }

  onShare = async (linkFotos) => {
    try {

      console.log(linkFotos);

      const result = await Share.share({
        title: 'Acesse o Link e Confira as Fotos do Meu Salto!',
        message: linkFotos,
      })

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };
  _validaConvidado = () => {
    if (this.state.loginConvidado == true) {
      return (
        <View>
          <Text style={{ fontSize: 20 }} >Você precisa estar autenticado para acessar esse modulo, deseja fazer isso agora ?</Text>
          <View style={{ marginTop: 10 }} >
            <Button dark onPress={ async () => { await AsyncStorage.clear(); this.props.navigation.navigate('Auth'); }} iconLeft>
            <Icon type="FontAwesome" name="sign-in"/>
              <Text>Fazer login</Text>
            </Button>
          </View>
        </View>
      );
    } else {
      return (
        <Text style={{ fontSize: 20 }} >Você Não Tem Fotos Vinculadas!</Text>
      )
    }
  }
  render() {
    const { loadDados, linkFotos, loginConvidado } = this.state;
    const tabTitle = this.props.route.params.tabTitle

    if (loadDados) {
      if (linkFotos != false) {
        return (
          <ScrollView contentContainerStyle={{ height: '100%', paddingBottom: 80 }}>
            <View>

              <Header hasBack titulo={tabTitle} navigation={this.props.navigation} />
              <View>

                {linkFotos.map((foto, index) => {
                  { console.log(foto) }
                  return (
                    <View key={index} style={{ borderWidth: 1, borderColor: '#c5c5c5', borderRadius: 20, marginHorizontal: 10, marginTop: 10 }} >
                      <View style={{ flex: 1, alignItems: "center", justifyContent: 'center', padding: 10 }}>
                        <View style={{ marginBottom: 10 }} >
                          <Text style={{ fontSize: 17, textAlign: "center" }} >Veja as fotos do seu salto do dia {converterMomento(foto.data)} !</Text>
                        </View>

                        <View>
                          <TouchableOpacity
                            onPress={() => { Linking.openURL(foto.foto) }}
                          >
                            <Image source={require('../assets/img/salto-midia.png')} />
                          </TouchableOpacity>
                        </View>
                      </View>

                      <View style={{ padding: 10, }} >
                        <Button dark onPress={() => { this.onShare(foto.foto) }} iconLeft>
                          <Icon name='share' />
                          <Text>Compartilhar</Text>
                        </Button>
                      </View>

                    </View>
                  )
                })}

              </View>
            </View>
          </ScrollView>
        );
      } else {
        return (
          <Container>
            <Content>
              <Header hasBack titulo={tabTitle} navigation={this.props.navigation} />
              <View style={{ flex: 1, marginTop: 50, alignItems: "center", justifyContent: 'center' }}>
                <View style={{ marginBottom: 50 }} >
                  {this._validaConvidado()}
                </View>
                <Image source={require('../assets/img/icone-sem-content.png')} />
              </View>
            </Content>
          </Container>
        );
      }

    } else {
      return (
        <Container>
          <Content contentContainerStyle={{ alignItems: 'center', justifyContent: 'center', flex: 1 }} >
            <ActivityIndicator />
          </Content>
        </Container>
      )
    }
  }

}
