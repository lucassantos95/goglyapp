import moment from 'moment';

// export const hosHOSTURL = 'http://go-fly-backend.com/default/images/';
// export const hosHOSTURL = 'http://qa-web.dedtechsolutions.com.br/gf/default/images/';
// export const hosHOSTURL = 'http://qa-web.dedtechsolutions.com.br/gofly/default/images/';
export const hosHOSTURL = 'https://app.goflyparaquedismo.com.br/default/images/';
// export const hosHOSTURL = 'http://ec2-34-199-237-159.compute-1.amazonaws.com/default/images/';

export function converterDatas(data) {
  var tratarData = data.split('-');
  tratarData = tratarData[2] + '-' + tratarData[1] + '-' + tratarData[0];
  return tratarData
}

export function converterHora(datatime) {
  var d = moment(datatime).format('HH');
  return d.toString();
}

export function converterMomento(datatime) {
  var d = moment(datatime).format('DD/MM/YYYY');
  return d.toString();
}

export function validaCpf(cpf) {

  cpf = cpf.replace('-', '');
  cpf = cpf.replace('.', '');
  cpf = cpf.replace('.', '');
  var numeros, digitos, soma, i, resultado, digitos_iguais;
  digitos_iguais = 1;
  if (cpf.length < 11) return false;
  for (i = 0; i < cpf.length - 1; i++)
    if (cpf.charAt(i) != cpf.charAt(i + 1)) {
      digitos_iguais = 0;
      break;
    }
  if (!digitos_iguais) {
    numeros = cpf.substring(0, 9);
    digitos = cpf.substring(9);
    soma = 0;
    for (i = 10; i > 1; i--) soma += numeros.charAt(10 - i) * i;
    resultado = soma % 11 < 2 ? 0 : 11 - (soma % 11);
    if (resultado != digitos.charAt(0)) return false;
    numeros = cpf.substring(0, 10);
    soma = 0;
    for (i = 11; i > 1; i--) soma += numeros.charAt(11 - i) * i;
    resultado = soma % 11 < 2 ? 0 : 11 - (soma % 11);
    if (resultado != digitos.charAt(1)) return false;
    return true;
  } else return false;

}

export function onlyNumber(cpf) {
  cpf = cpf.replace('-', '');
  cpf = cpf.replace('.', '');
  cpf = cpf.replace('.', '');
  return cpf;
}

export function numeroParaMoeda(n, c, d, t){
  c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
  return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

