import axios from 'axios';

const api = axios.create({
    // baseURL: 'http://go-fly-backend.com/api',
    // baseURL: 'http://qa-web.dedtechsolutions.com.br/gf/api',
    baseURL: 'https://app.goflyparaquedismo.com.br/api',
    // baseURL: 'http://ec2-34-199-237-159.compute-1.amazonaws.com/api',
    // baseURL: 'http://qa-web.dedtechsolutions.com.br/gofly/api',
});

export default api;