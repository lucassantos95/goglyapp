import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import SignInScreen from '../pages/SignIn';
import AuthLoadingScreen from '../pages/AuthLoadingScreen';

import CadastroAtleta from '../pages/Atleta/Cadastro'
import CadastroAtleta2 from '../pages/Atleta/Cadastro2'
import CadastroCliente from '../pages/Cliente/Cadastro'
import TermoDeUso from '../pages/TermoDeUso';
import FinalizacaoCadastro from '../pages/FinalizacaoCadastro';
import FazerLogin from '../pages/FazerLogin';
import RecuperarSenha from '../pages/RecuperarSenha';
import DecolagemDetalhes from '../pages/DecolagemDetalhes';
import NewsDetalhes from '../pages/NewsDetalhes';
import Perfil from '../pages/Perfil';


import News from '../pages/News';
import Tempo from '../pages/Tempo';
import Decolagem from '../pages/Decolagem';
import Saltos from '../pages/Saltos';
import Store from '../pages/Store';


import Agenda from '../pages/Agenda';
import Midia from '../pages/Midia';

import Produto from '../pages/Produto';

import HomeScreenTabNavigator from '../pages/TabNavigator/HomeScreenTabNavigator'

function Routes() {

  const AppStack = createStackNavigator();

  function AppStackScreen() {
    return (
      <AppStack.Navigator>
        <AppStack.Screen
          name="HomeScreenTabNavigator"
          component={HomeScreenTabNavigator}
          options={{
            headerShown: false,
          }}
        />
        <AppStack.Screen
          name="Tempo"
          component={Tempo}
          options={{
            headerShown: false,
          }}
        />
        <AppStack.Screen
          name="NewsDetalhes"
          component={NewsDetalhes}
          options={{
            headerShown: false,
          }}
        />
        <AppStack.Screen
          name="News"
          component={News}
          options={{
            headerShown: false,
          }}
        />
        <AppStack.Screen
          name="Decolagem"
          component={Decolagem}
          options={{
            headerShown: false,
          }}
        />
        <AppStack.Screen
          name="DecolagemDetalhes"
          component={DecolagemDetalhes}
          options={{
            headerShown: false,
          }}
        />
        <AppStack.Screen
          name="Saltos"
          component={Saltos}
          options={{
            headerShown: false,
          }}
        />
        <AppStack.Screen
          name="Store"
          component={Store}
          options={{
            headerShown: false,
          }}
        />
        <AppStack.Screen
          name="Agenda"
          component={Agenda}
          options={{
            headerShown: false,
          }}
        />
        <AppStack.Screen
          name="Midia"
          component={Midia}
          options={{
            headerShown: false,
          }}
        />
        <AppStack.Screen
          name="Perfil"
          component={Perfil}
          options={{
            headerShown: false,
          }}
        />
        <AppStack.Screen
          name="Produto"
          component={Produto}
          options={{
            headerShown: false,
          }}
        />
      </AppStack.Navigator>
    );
  }

  const AuthStack = createStackNavigator();

  function AuthStackScreen() {
    return (
      <AuthStack.Navigator>
        <AuthStack.Screen
          name="SignIn"
          component={SignInScreen}
          options={{
            headerShown: false,
          }}
        />
        <AuthStack.Screen
          name="FazerLogin"
          component={FazerLogin}
          options={{
            headerShown: false,
          }}
        />
        <AuthStack.Screen
          name="RecuperarSenha"
          component={RecuperarSenha}
          options={{
            headerShown: false,
          }}
        />
        <AuthStack.Screen
          name="CadastroCliente"
          component={CadastroCliente}
          options={{
            headerShown: false,
          }}
        />
        <AuthStack.Screen
          name="CadastroAtleta2"
          component={CadastroAtleta2}
          options={{
            headerShown: false,
          }}
        />
        <AuthStack.Screen
          name="CadastroAtleta"
          component={CadastroAtleta}
          options={{
            headerShown: false,
          }}
        />
        <AuthStack.Screen
          name="TermoDeUso"
          component={TermoDeUso}
          options={{
            headerShown: false,
          }}
        />
        <AuthStack.Screen
          name="FinalizacaoCadastro"
          component={FinalizacaoCadastro}
          options={{
            headerShown: false,
          }}
        />
        <AuthStack.Screen
          name="DecolagemDetalhes"
          component={DecolagemDetalhes}
          options={{
            headerShown: false,
          }}
        />
      </AuthStack.Navigator>
    );
  }

  const ContentStack = createStackNavigator();
  return (
    <NavigationContainer>
      <ContentStack.Navigator initialRouteName={'AuthLoading'}>
      <ContentStack.Screen
          name="AuthLoading"
          component={AuthLoadingScreen}
          options={{
            headerShown: false,
          }}
        />
        <ContentStack.Screen
          name="Auth"
          component={AuthStackScreen}
          options={{
            headerShown: false,
          }}
        />
        <ContentStack.Screen
          name="App"
          component={AppStackScreen}
          options={{
            headerShown: false,
          }}
        />
      </ContentStack.Navigator>
    </NavigationContainer>
  )
}

export default Routes;