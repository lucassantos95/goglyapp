import React from 'react';

import { View, Text, Image } from 'react-native';

import { Container, Content } from 'native-base';

function NoContent({ name }) {
  return (
    <Container>
      <Content>
        <View
          style={{
            flex: 1,
            marginTop: 50,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View style={{ marginBottom: 50 }}>
            <Text style={{ fontSize: 20 }}>
              {`Não Temos ${name} no Momento!`}
            </Text>
          </View>
          <Image source={require('../../assets/img/icone-sem-content.png')} />
        </View>
      </Content>
    </Container>
  );
}

export default NoContent;
