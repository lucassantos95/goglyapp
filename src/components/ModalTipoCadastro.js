import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';
import Modal from "react-native-modal";


export default class NewRepoModal extends Component {

    state = {
        isModalVisible: false
    };


    renderModalContent = () => (
        <View style={styles.modalContent}>
            <Text style={{ textAlign:'center' }} >Selecione  o tipo de usuario que melhor corresponde ao uso do app</Text>
            <View style={styles.buttonContent} >

                <TouchableOpacity style={styles.buttonAtleta} onPress={this.props.openAtleta}>
                    <Text style={styles.textAtleta} >ATLETA</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.buttonCliente} onPress={this.props.openCliente}>
                    <Text style={styles.textCliente} >CLIENTE</Text>
                </TouchableOpacity>

            </View>
        </View>
    );

    render() {
        return (

            <Modal
                isVisible={this.props.isVisible}
                onBackdropPress={this.props.onCancel}
                animationIn="slideInLeft"
                animationOut="slideOutRight"
           
            >
                {this.renderModalContent()}
            </Modal>



        );

    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    button: {
        backgroundColor: "lightblue",
        padding: 12,
        margin: 16,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    modalContent: {
        backgroundColor: "white",
        padding: 22,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    buttonContent: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',

    },
  
  
    buttonContainer: {
        alignItems: 'center',
        padding: 10
    },
    buttonAtleta: {
        alignItems: 'center',
        backgroundColor: '#FEEE00',
        margin: 10,
        padding: 20
    },
    buttonCliente: {
        alignItems: 'center',
        backgroundColor: '#000000',
        margin: 10,
        padding: 20
    },
    textAtleta: {
        color: '#000000',
        fontWeight:'bold',
        fontSize:20
    },
    textCliente: {
        color: '#FEEE00',
        fontWeight:'bold',
        fontSize:20
    },
});