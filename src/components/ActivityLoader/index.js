import React from 'react';

import { ActivityIndicator } from 'react-native';

import { Container, Content } from 'native-base';

function ActivityLoader() {
  return (
    <Container>
      <Content
        contentContainerStyle={{
          alignItems: 'center',
          justifyContent: 'center',
          flex: 1,
        }}>
        <ActivityIndicator />
      </Content>
    </Container>
  );
}

export default ActivityLoader;
