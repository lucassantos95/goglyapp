import React from 'react';
import Loading from 'react-native-whc-loading';

function Loader({ show }) {
  return (
    <Loading
      show={show}
      backgroundColor={'#00000096'}
      indicatorColor={'#fff'}
    />
  );
}

export default Loader;
