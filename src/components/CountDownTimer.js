import React, { Component } from 'react';
import {
  Text,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';


import TimerCountdown from 'react-native-timer-countdown-lucas';
import moment from 'moment';

export default class CountDownTimer extends Component {

  constructor() {
    super()
    this.state = {
      timer: '',

    };
  }

  static navigationOptions = {
    title: 'Lots of features here',
  };

  onTimeElapsed = (decolagem_id) => {
    alert(decolagem_id)
    this.props.excuteTimer(decolagem_id);
  }

  componentDidMount = () => {

    var startDate = moment(new Date());
    // Do your operations
    var endDate = moment(this.props.dateLimit);
  
    var duration = moment.duration(endDate.diff(startDate));
    var seconds = duration.asSeconds();
    console.log(seconds)
    if(seconds < 0)
        seconds = 0;

    this.setState({
      timer: seconds
    })
    // console.log(this.props.dateLimit)
  }
  render() {
    // console.log(this.state.timer)
    return (
     <TimerCountdown
        initialSecondsRemaining={1000 * this.state.timer}
        // onTick={secondsRemaining => console.log("tick", secondsRemaining)}
        // onTimeElapsed={() => { this.onTimeElapsed(this.props.decolagem_id) }}
        allowFontScaling={true}
        style={{ fontSize: 30 }}
      />
    );
  }

}
