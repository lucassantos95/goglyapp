import React, { Component } from 'react';
import { Image, StyleSheet, Alert, Platform, TouchableOpacity, AsyncStorage, View } from 'react-native';
import api from '../services/Api';
import { Header, Title, Left, Right, Body, Thumbnail } from 'native-base';
import { hosHOSTURL } from '../services/Helpers';
export default class HeaderSemAvatar extends Component {

    constructor(props){
        super(props);
        this.state = {
            fotoPerfil:'',
            loginConvidado:false
        }
    }
 
    logout = async () => {
        await AsyncStorage.clear();
        this.props.navigation.navigate('Auth');
    }

    openPerfil = async () => {

        if(this.state.loginConvidado == true){
            return Alert.alert(
                'Go Fly',
                'Você precisa estar autenticado para acessar esse modulo, deseja fazer isso agora ?',
                [
                    {
                        text: 'Não',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    {
                        text: 'Sim', onPress: async () => {
                            await AsyncStorage.clear();
                            this.props.navigation.navigate('Auth');
                        }
                    },
                ],
                { cancelable: false },
            );
        }
        this.props.navigation.navigate('Perfil');
        // await AsyncStorage.clear();
        // this.props.navigation.navigate('Auth');
    }

    componentDidMount() {
        this._loadPerfilData();
    }
    _loadPerfilData = async () => {

        const usuario_dados = await AsyncStorage.getItem('Usuario');
        var usuario = JSON.parse(usuario_dados);

        console.log(usuario+' dados user perfil');
        if(usuario != null){
        const userTipo = await AsyncStorage.getItem('Usuario.tipo');
            
        var dados = { params:{ usuario_id: usuario.id, userTipo: userTipo }};

        const response = await api.get('/login/get-users-dados', dados);

        if (response.data.status == "Success") {
            var usuario = response.data.usuario;
            const handlePhoto = !usuario.foto_perfil ? 'https://cdn.iconscout.com/icon/free/png-256/account-profile-avatar-man-circle-round-user-30452.png' : `${hosHOSTURL}foto_perfil/${usuario.foto_perfil}`
            console.log(handlePhoto)
            console.log(usuario.foto_perfil)
            this.setState({
                fotoPerfil: handlePhoto
            })
        }
        }else{
            this.setState({
                loginConvidado:true,
                fotoPerfil: 'https://cdn.iconscout.com/icon/free/png-256/account-profile-avatar-man-circle-round-user-30452.png'
            }) 
        }
        this.setState({
            loadDados: true
        })
    }

    actionBack = (action) => {

        switch (action) {
            case 'decolagemList':
              this.props.navigation.goBack();
                break;
        
            default:
            this.props.navigation.goBack();
                break;
        }
    }
    render() {
        
        return (
            <Header androidStatusBarColor="#000" style={styles.barraHeader} >
                <Left>
                {!this.props.hasBack ? (
                    <TouchableOpacity style={{ padding: (Platform.OS === 'android') ? 0 : 20, }} onPress={() => { this.actionBack(this.props.action) }} >
                        <Image source={require('../assets/img/voltar.png')} />
                    </TouchableOpacity>
                ) : (
                    <View></View>
                )}
                     </Left>
                    
               
                <Body>
                    <Title style={{ color: '#000000' }} >{this.props.titulo}</Title>
                </Body>
                <Right>
                    <TouchableOpacity onPress={this.openPerfil} >
                        <Thumbnail style={{ width: 40, height: 40, borderWidth:1,borderRadius: 20,borderColor:'#000' }} source={{ uri: this.state.fotoPerfil}} />
                    </TouchableOpacity>
                </Right>
            </Header>
        );
    }
}

const styles = StyleSheet.create({

    barraHeader: {
        backgroundColor: '#FFFFFF',
        marginTop: (Platform.OS === 'android') ? 0 : 0,
        borderBottomWidth: 2,
        borderBottomColor: '#e0e0e0',
        height: 70
    }

});