import React, { Component } from 'react';
import { Image, StyleSheet, Platform, TouchableOpacity } from 'react-native';

import { Header, Title, Left, Right, Body } from 'native-base';

export default class HeaderSemAvatar extends Component {
  render() {
    return (
      <Header androidStatusBarColor="#000" style={styles.barraHeader} >
        <Left>
          <TouchableOpacity style={{ padding: (Platform.OS === 'android') ? 0 : 20, }} onPress={() => { this.props.navigation.goBack() }} >
            <Image source={require('../assets/img/voltar.png')} />

          </TouchableOpacity>
        </Left>
        <Body>
          <Title style={{ color: '#000000' }} >{this.props.titulo}</Title>
        </Body>
        <Right />
      </Header>
    );
  }
}

const styles = StyleSheet.create({

  barraHeader: {
    backgroundColor: '#FFFFFF',
    marginTop: (Platform.OS === 'android') ? 0 : 0,
    borderBottomWidth: 2,
    borderBottomColor: '#e0e0e0'
  }

});