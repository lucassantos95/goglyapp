import React from 'react';

import { View, Linking } from 'react-native';

import { Card, CardItem, Body, Text, Thumbnail, Button } from 'native-base';
import { hosHOSTURL, numeroParaMoeda } from '../../services/Helpers';

function CardProduct({ produto }) {
  return (
    <Card>
      <CardItem style={{ flexBasis: 0 }}>
        <View>
          <Thumbnail
            style={{ width: 140, height: 150, resizeMode: 'stretch' }}
            square
            source={{ uri: hosHOSTURL + '/store/' + produto.imagem }}
          />
        </View>
        <Body>
          <View
            style={{ flexDirection: 'column', height: 150, paddingLeft: 15 }}>
            <View style={{ marginBottom: 10 }}>
              <Text>{produto.titulo}</Text>
            </View>
            <View style={{ flex: 2 }}>
              <Text note numberOfLines={1}>
                {produto.descricao}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'flex-start',
                justifyContent: 'flex-start',
              }}>
              <View style={{ marginLeft: 5 }}>
                <Text>R$ {numeroParaMoeda(produto.valor)}</Text>
              </View>
              <View style={{ marginLeft: 0, marginTop: 5 }}>
                <Button
                  small
                  onPress={() => {
                    Linking.openURL(produto.link_anuncio);
                  }}
                  style={{ backgroundColor: '#FEEE00' }}
                  rounded>
                  <Text style={{ color: '#000' }}>COMPRAR</Text>
                </Button>
              </View>
            </View>
          </View>
        </Body>
      </CardItem>
    </Card>
  );
}

export default CardProduct;
