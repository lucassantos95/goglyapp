import React, { Component } from 'react';
import Routes from './src/navigation/Routes';
import * as Font from 'expo-font';

export default class App extends Component {
  state = {
    fontLoaded: false
  }
  async componentDidMount() {
    await Font.loadAsync({
      'Arial': require('./src/assets/fonts/Arial.ttf'),
      'Roboto_medium': require('./src/assets/fonts/Roboto-Medium.ttf')
    });
    this.setState({ fontLoaded: true });
  }

  render(){
      if (!this.state.fontLoaded) return null;
    return (
      <Routes /> 
    );
  }
}
